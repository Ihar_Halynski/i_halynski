<%@page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" import="com.servlets.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html style="text-align: center">
<head>
    <title>Order page</title>
</head>
<body>
<c:set var="selectedProducts" value="${sessionScope.selectedProducts}"/>
<c:set var="catalog" value="${sessionScope.catalog}"/>
<h2>Hello ${sessionScope.currentUser.getLogin()}</h2>
<c:if test="${sessionScope.selectedProducts != null}">
    <h2>You have already chosen:</h2>
</c:if>
<c:if test="${sessionScope.selectedProducts == null}">
    <h2>Make you order:</h2>
</c:if>
<c:if test="${selectedProducts != null}">
    <c:set var="counter" value="1"/>
    <c:forEach var="selectedProduct" items="${selectedProducts}">
        <h4>${counter}) ${selectedProduct.toString()}</h4>
        <c:set var="counter" value="${counter = counter + 1}"/>
    </c:forEach>
</c:if>
<form action="forward" method="post">
    <select name="product">
        <c:forEach var="availableProduct" items="${catalog.getAvailableProducts()}">
            <option value="${availableProduct.getTitle()}"><c:out value="${availableProduct.toString()}"/></option>
        </c:forEach>
    </select>
    <input type="submit" name="submit" value="Submit"/>
    <input type="submit" name="addItem" value="Add item"/>
</form>
</body>
</html>
