<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html style="text-align: center">
<head>
    <title>Check page</title>
</head>
<body>
<h2>Dear ${sessionScope.currentUser.getLogin()}, you order:</h2>
<c:set var="counter" value="1"/>
<c:forEach var="product" items="${sessionScope.selectedProducts}">
    <h3>${counter}) ${product.toString()}</h3>
    <c:set var="counter" value="${counter = counter + 1}"/>
</c:forEach>
<h3>Total: ${sessionScope.total}</h3>
</body>
</html>