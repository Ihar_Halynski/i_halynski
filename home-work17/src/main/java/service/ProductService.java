package service;

import database.H2DB;
import dao.ProductDAO;
import entity.Product;

import java.sql.*;
import java.util.HashSet;

public class ProductService extends H2DB implements ProductDAO {

    @Override
    public Product getById(Integer id) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "SELECT ID, TITLE, PRICE FROM PRODUCTS WHERE ID=?";
        Product product = new Product();
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                product.setId((resultSet.getInt("ID")));
                product.setTitle(resultSet.getString("TITLE"));
                product.setPrice(resultSet.getDouble("PRICE"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return product;

    }

    @Override
    public HashSet<Product> getAll() throws SQLException {
        Connection connection = getConnection();
        Statement statement = null;
        String sql = "SELECT ID, TITLE, PRICE FROM PRODUCTS";
        HashSet<Product> products = new HashSet<>();
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet != null) {
                while (resultSet.next()) {
                    Product product = new Product();
                    product.setId(resultSet.getInt("ID"));
                    product.setTitle(resultSet.getString("TITLE"));
                    product.setPrice(resultSet.getDouble("PRICE"));
                    products.add(product);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return products;
    }
}
