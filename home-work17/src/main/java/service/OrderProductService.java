package service;

import database.H2DB;
import dao.OrderProductDAO;
import entity.OrderProduct;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderProductService extends H2DB implements OrderProductDAO {

    @Override
    public void add(OrderProduct orderProduct) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO ORDERS_PRODUCTS (ORDER_ID, PRODUCT_ID) VALUES(?, ?)";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, orderProduct.getOrderId());
            preparedStatement.setInt(2, orderProduct.getProductId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public List<OrderProduct> getByOrderId(Integer orderId) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "SELECT ID, ORDER_ID, PRODUCT_ID FROM ORDERS_PRODUCTS WHERE ORDER_ID=?";
        List<OrderProduct> orderProducts = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, orderId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet != null) {
                while (resultSet.next()) {
                    OrderProduct OrderProduct = new OrderProduct();
                    OrderProduct.setId(resultSet.getInt("ID"));
                    OrderProduct.setOrderId(resultSet.getInt("ORDER_ID"));
                    OrderProduct.setProductId(resultSet.getInt("PRODUCT_ID"));
                    orderProducts.add(OrderProduct);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return orderProducts;
    }
}
