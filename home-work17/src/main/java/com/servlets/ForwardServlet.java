package com.servlets;

import database.H2DB;
import entity.Order;
import entity.OrderProduct;
import entity.Product;
import entity.User;
import service.OrderProductService;
import service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ForwardServlet extends DispatcherServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String submit = request.getParameter("submit");
        String addItem = request.getParameter("addItem");
        List<Product> selectedProducts = null;
        if (submit != null) {
            submit(session, selectedProducts);
            forward("/checkpage.jsp", request, response);
        }
        if (addItem != null) {
            addItem(request, session, selectedProducts);
            super.forward("/catalogpage.jsp", request, response);
        } else {
            super.forward("/errorpage.jsp", request, response);
        }
    }

    private void submit(HttpSession session, List<Product> selectedProducts) {
        selectedProducts = (ArrayList<Product>) session.getAttribute("selectedProducts");
        double total = 0;
        if (selectedProducts != null) {
            total = calculateTotal(selectedProducts);
        }
        session.setAttribute("total", total);
        Order order = new Order();
        order.setTotalPrice(total);
        User currentUser = (User) session.getAttribute("currentUser");
        order.setUserId(currentUser.getId());
        OrderService orderService = new OrderService();
        OrderProductService orderProductService = new OrderProductService();
        try {
            orderService.add(order);
            order = orderService.getByUserId(currentUser.getId());
            OrderProduct orderProduct = null;
            for (Product p : selectedProducts) {
                orderProduct = new OrderProduct();
                orderProduct.setProductId(p.getId());
                orderProduct.setOrderId(order.getId());
                orderProductService.add(orderProduct);
            }
            new H2DB().save();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addItem(HttpServletRequest request, HttpSession session, List<Product> selectedProducts) {
        Catalog catalog = (Catalog) session.getAttribute("catalog");
        Product selectedProduct = null;
        String selected = request.getParameter("product");
        if (catalog != null && selected != null) {
            selectedProduct = catalog.getProductByTitle(selected);
        }
        if (selectedProduct != null) {
            selectedProducts = (ArrayList<Product>) session.getAttribute("selectedProducts");
            if (selectedProducts != null) {
                selectedProducts.add(selectedProduct);
            } else {
                selectedProducts = new ArrayList<>();
                selectedProducts.add(selectedProduct);
            }
            session.setAttribute("selectedProducts", selectedProducts);
        }
    }

    private double calculateTotal(List<Product> products) {
        double result = 0;
        for (Product p : products) {
            result = result + p.getPrice();
        }
        return result;
    }
}
