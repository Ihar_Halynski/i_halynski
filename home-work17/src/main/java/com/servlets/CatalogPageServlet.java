package com.servlets;

import database.H2DB;
import entity.Order;
import entity.OrderProduct;
import entity.Product;
import entity.User;
import service.OrderProductService;
import service.OrderService;
import service.ProductService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CatalogPageServlet extends DispatcherServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean agreeTermsOfService = Boolean.parseBoolean(request.getParameter("agreeTermsOfService"));
        if (agreeTermsOfService) {
            try {
                HttpSession session = request.getSession();
                Connection connection = (Connection) session.getAttribute("connection");
                if (connection == null) {
                    connection = new H2DB().init();
                    session.setAttribute("connection", connection);
                }
                String userName = request.getParameter("userName");
                if (userName != null) {
                    request.getSession(false).invalidate();
                    session = request.getSession();
                    session.setAttribute("connection", connection);
                }
                workWithDB(session, userName);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            forward("/catalogpage.jsp", request, response);
        } else {
            forward("/errorpage.jsp", request, response);
        }
    }

    private void workWithDB(HttpSession session, String userName) throws SQLException {
        OrderService orderService = new OrderService();
        UserService userService = new UserService();
        OrderProductService orderProductService = new OrderProductService();
        ProductService productService = new ProductService();
        User user = userService.getByLogin(userName);
        if (user.getLogin() != null) {
            Order order = orderService.getByUserId(user.getId());
            List<OrderProduct> orderProducts = orderProductService.getByOrderId(order.getId());
            List<Product> selectedProducts = new ArrayList<>();
            for (OrderProduct orderProduct : orderProducts) {
                Product product = productService.getById(orderProduct.getProductId());
                selectedProducts.add(product);
            }
            session.setAttribute("selectedProducts", selectedProducts);
        } else {
            user = new User();
            user.setLogin(userName);
            userService.add(user);
            user = userService.getByLogin(userName);
        }
        session.setAttribute("currentUser", user);
        Catalog catalog = new Catalog();
        HashSet<Product> products = productService.getAll();
        if (products != null) {
            catalog.setAvailableProducts(products);
            session.setAttribute("catalog", catalog);
        }
    }
}
