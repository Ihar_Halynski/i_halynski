package com.servlets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import entity.Product;

public class Catalog {

    private HashSet<Product> availableProducts = new HashSet<>();

    public void setAvailableProducts(HashSet<Product> availableProducts) {
        this.availableProducts = availableProducts;
    }

    public HashSet<Product> getAvailableProducts() {
        return availableProducts;
    }

    public ArrayList<Product> getAvailableProductsAsStringList() {
        ArrayList<Product> result = new ArrayList<>();
        for (Product p : availableProducts) {
            result.add(p);
        }
        return result;
    }

    public Product getProductByTitle(String name) {
        for (Product p : availableProducts) {
            if (p.getTitle().equals(name)) {
                return p;
            }
        }
        return null;
    }
}
