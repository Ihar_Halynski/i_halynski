package dao;

import entity.OrderProduct;

import java.sql.SQLException;
import java.util.List;

public interface OrderProductDAO {

    void add(OrderProduct orderProduct) throws SQLException;

    List<OrderProduct> getByOrderId(Integer orderId) throws SQLException;

}
