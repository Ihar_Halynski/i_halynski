package dao;

import entity.Product;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;

public interface ProductDAO {

    Product getById(Integer id) throws SQLException;

    HashSet<Product> getAll() throws SQLException;

}
