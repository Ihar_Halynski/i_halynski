package dao;

import entity.Order;
import entity.User;

import java.sql.SQLException;

public interface OrderDAO {

    void add(Order order) throws SQLException;

    Order getByUserId(Integer userId) throws SQLException;
}
