package dao;

import entity.User;

import java.sql.SQLException;

public interface UserDAO {

    void add(User user) throws SQLException;

    User getByLogin(String login) throws SQLException;
}
