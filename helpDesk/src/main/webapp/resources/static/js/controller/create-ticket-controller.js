app.controller('CreateTicketController', ['$scope', '$window', '$http', 'CategoryService', 'UrgencyService', function ($scope, $window, $http, CategoryService, UrgencyService) {
  $scope.test = false
  $scope.isFileExtensionNotValid = false
  $scope.isFileSizeNotValid = false
  CategoryService.getCategories()
    .then(
      function (response) {
        $scope.categories = response
        $scope.category = response[0]
      },
      function (error) {
        console.log(error)
      }
    )

  UrgencyService.getUrgency()
    .then(
      function (response) {
        $scope.urgency = response
        $scope.selectedUrgency = response[0]
      },
      function (error) {
        console.log(error)
      }
    )

  $scope.checkFiles = function checkFiles (input) {
    $scope.file = input.files
    var file
    try {
      file = input.files[0]
      console.log(file.name)
      if (file.size < 1024 * 1024 * 5) {
        $scope.isFileSizeNotValid = false
        console.log('Valid')
      } else {
        $scope.isFileSizeNotValid = true
        console.log('NotValid')
      }
      var parts = file.name.split('.')
      var ext = parts[parts.length - 1].toLowerCase()
      switch (ext) {
        case 'pdf':
        case 'doc':
        case 'docx':
        case 'png':
        case 'jpeg':
        case 'jpg':
          $scope.isFileExtensionNotValid = false
          $scope.$apply()
          console.log('Extension is valid')
          return
      }
      console.log('Extension is not valid')
      $scope.isFileExtensionNotValid = true
      $scope.$apply()
    } catch (error) {
      console.log(error.message)
    }
  }

  function saveNewTicket (state) {
    console.log($scope.file)
    var name = $scope.name
    var selectedUrgency = $scope.selectedUrgency
    var description = $scope.description !== undefined ? $scope.description : ''
    var desiredDate = $scope.date !== undefined ? new Date($scope.date).getTime() + 86400000 : new Date().getTime()
    var comment = $scope.comment !== undefined ? $scope.comment : ''
    var data = {
      state: state,
      categoryId: $scope.category.id,
      name: name,
      urgency: selectedUrgency,
      description: description,
      desiredResolutionDate: desiredDate,
      comment: comment
    }
    var fd = new FormData()
    angular.forEach($scope.file, function (value, key) {
      fd.append('files', value)
    })
    fd.append('ticketDto', JSON.stringify(data))
    var url = '/tickets/create-new'
    return $http({
      method: 'POST',
      url: url,
      headers: {
        'Content-Type': undefined
      },
      data: fd,
      transformRequest: function (data, headersGetterFunction) {
        return data
      }
    })
      .then(
        function (response) {
          $window.location.href = '/home'
        },
        function (error) {
          console.log(error)
          $window.location.href = '/home'
        }
      )
  }

  $scope.saveAsDraft = function (value) {
    if (value && !$scope.isFileExtensionNotValid && !$scope.isFileSizeNotValid) {
      saveNewTicket('DRAFT')
    }
  }
  $scope.saveNew = function (value) {
    if (value && !$scope.isFileExtensionNotValid && !$scope.isFileSizeNotValid) {
      saveNewTicket('NEW')
    }
  }
}])
