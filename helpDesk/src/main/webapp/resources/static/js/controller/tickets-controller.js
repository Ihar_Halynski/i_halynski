app.controller('TicketsController', ['$scope', 'TicketsService', 'UserService', function ($scope, TicketsService, UserService) {
  $scope.sortByName = true
  var tickets, currentUser

  UserService.getCurrentUser()
    .then(
      function (response) {
        currentUser = response
        $scope.currentUser = currentUser
      }
    )

  TicketsService.getAllTickets()
    .then(
      function (response) {
        tickets = response
        getAllTickets()
      }
    )

  function getAllTickets () {
    var ticketList = []
    try {
      if (currentUser.role === 'EMPLOYEE') {
        for (var i = 0; i < tickets.length; i++) {
          if (tickets[i].owner.email === currentUser.email) {
            ticketList.push(tickets[i])
          }
        }
      }
      if (currentUser.role === 'MANAGER') {
        for (var i = 0; i < tickets.length; i++) {
          if (tickets[i].owner.email === currentUser.email || tickets[i].state === 'NEW' ||
              (tickets[i].approver !== null && tickets[i].approver.email === currentUser.email)) {
            ticketList.push(tickets[i])
          }
        }
      }
      if (currentUser.role === 'ENGINEER') {
        for (var i = 0; i < tickets.length; i++) {
          if (tickets[i].state === 'APPROVED' || (tickets[i].assignee !== null && tickets[i].assignee.email === currentUser.email)) {
            ticketList.push(tickets[i])
          }
        }
      }
      $scope.tickets = ticketList
    } catch (error) {
      console.log(error.message)
    }
  }

  $scope.showAllTickets = function () {
    getAllTickets()
  }

  var getMyTickets = function () {
    var myTickets = []
    try {
      if (currentUser.role === 'EMPLOYEE') {
        for (var i = 0; i < tickets.length; i++) {
          if (tickets[i].owner.email === currentUser.email) {
            myTickets.push(tickets[i])
          }
        }
      }
      if (currentUser.role === 'MANAGER') {
        for (var i = 0; i < tickets.length; i++) {
          if (tickets[i].owner.email === currentUser.email ||
              (tickets[i].approver !== null && tickets[i].approver.email === currentUser.email)) {
            myTickets.push(tickets[i])
          }
        }
      }
      if (currentUser.role === 'ENGINEER') {
        for (var i = 0; i < tickets.length; i++) {
          if (tickets[i].assignee !== null && tickets[i].assignee.email === currentUser.email) {
            myTickets.push(tickets[i])
          }
        }
      }
      $scope.tickets = myTickets
    } catch (error) {
      console.log(error.message)
    }
  }

  $scope.showMyTickets = function () {
    getMyTickets()
  }

  $scope.isSubmitAvailable = function (ticket) {
    if ((ticket.state === 'DRAFT' && currentUser.role === 'EMPLOYEE' && ticket.owner.email === currentUser.email) ||
          (ticket.state === 'DRAFT' && currentUser.role === 'MANAGER' && ticket.owner.email === currentUser.email) ||
          (ticket.state === 'DECLINED' && currentUser.role === 'EMPLOYEE' && ticket.owner.email === currentUser.email) ||
          (ticket.state === 'DECLINED' && currentUser.role === 'MANAGER' && ticket.owner.email === currentUser.email)) {
      return true
    }
  }

  $scope.isApproveAvailable = function (ticket) {
    if (ticket.state === 'NEW' && currentUser.role === 'MANAGER' && ticket.owner.email != currentUser.email) {
      return true
    }
  }

  $scope.isCancelAvailable = function (ticket) {
    if ((ticket.state === 'DRAFT' && currentUser.role === 'EMPLOYEE') ||
            (ticket.state === 'DRAFT' && currentUser.role === 'MANAGER' && ticket.owner.email === currentUser.email) ||
            (ticket.state === 'NEW' && currentUser.role === 'MANAGER' && ticket.owner.email != currentUser.email) ||
            (ticket.state === 'APPROVED' && currentUser.role === 'ENGINEER') ||
            (ticket.state === 'DECLINED' && currentUser.role === 'EMPLOYEE' && ticket.owner.email === currentUser.email) ||
            (ticket.state === 'DECLINED' && currentUser.role === 'MANAGER' && ticket.owner.email === currentUser.email)) {
      return true
    }
  }

  $scope.isDeclineAvailable = function (ticket) {
    if (ticket.state === 'NEW' && currentUser.role === 'MANAGER' && ticket.owner.email != currentUser.email) {
      return true
    }
  }

  $scope.isAssignAvailable = function (ticket) {
    if (ticket.state === 'APPROVED' && currentUser.role === 'ENGINEER') {
      return true
    }
  }

  $scope.isDoneAvailable = function (ticket) {
    if (ticket.state === 'IN_PROGRESS' && currentUser.email === ticket.assignee.email) {
      return true
    }
  }

  $scope.doAction = function (ticket, action) {
    if (action === 'submit' && TicketsService.submit(ticket.id)) {
      ticket.state = 'NEW'
    }
    if (action === 'approve' && TicketsService.approve(ticket.id)) {
      ticket.state = 'APPROVED'
    }
    if (action === 'cancel' && TicketsService.cancel(ticket.id)) {
      ticket.state = 'CANCELED'
    }
    if (action === 'decline' && TicketsService.decline(ticket.id)) {
      ticket.state = 'DECLINED'
    }
    if (action === 'assign' && TicketsService.assign(ticket.id)) {
      ticket.state = 'IN_PROGRESS'
      ticket.assignee = currentUser
      action = 'done'
      return action
    }
    if (action === 'done' && TicketsService.done(ticket.id)) {
      ticket.state = 'DONE'
    }
  }

  $scope.switchSortType = function (selectedSortType) {
    if (selectedSortType === 'id') {
      sortTable(0)
      $scope.selectedSort = 'id'
    }
    if (selectedSortType === 'name') {
      sortTableByName(1)
      $scope.selectedSort = 'name'
    }
    if (selectedSortType === 'desiredDate') {
      sortTableByDate(2)
      $scope.selectedSort = 'desiredDate'
    }
    if (selectedSortType === 'urgency') {
      sortTableByUrgency(3)
      $scope.selectedSort = 'urgency'
    }
    if (selectedSortType === 'status') {
      sortTable(4)
      $scope.selectedSort = 'status'
    }
  }

  var sortTableByName = function sortTableByName (columnNumber) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0
    table = document.getElementById('ticketsTable')
    switching = true
    dir = 'asc'
    while (switching) {
      switching = false
      rows = table.rows
      for (i = 1; i < (rows.length - 1); i++) {
        shouldSwitch = false
        x = rows[i].getElementsByTagName('td')[columnNumber].getElementsByTagName('div')[0].getElementsByTagName('a')[0]
        y = rows[i + 1].getElementsByTagName('td')[columnNumber].getElementsByTagName('div')[0].getElementsByTagName('a')[0]
        if (dir == 'asc') {
          if (x.innerHTML.toLowerCase().localeCompare(y.innerHTML.toLowerCase()) > 0) {
            shouldSwitch = true
            break
          }
        } else if (dir == 'desc') {
          if (x.innerHTML.toLowerCase().localeCompare(y.innerHTML.toLowerCase()) < 0) {
            shouldSwitch = true
            break
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i])
        switching = true
        switchcount++
      } else {
        if (switchcount == 0 && dir == 'asc') {
          dir = 'desc'
          switching = true
        }
      }
    }
  }

  var sortTable = function sortTable (columnNumber) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0
    table = document.getElementById('ticketsTable')
    switching = true
    dir = 'asc'
    while (switching) {
      switching = false
      rows = table.rows
      for (i = 1; i < (rows.length - 1); i++) {
        shouldSwitch = false
        x = rows[i].getElementsByTagName('td')[columnNumber]
        y = rows[i + 1].getElementsByTagName('td')[columnNumber]
        if (dir == 'asc') {
          if (x.innerHTML.toLowerCase().localeCompare(y.innerHTML.toLowerCase()) > 0) {
            shouldSwitch = true
            break
          }
        } else if (dir == 'desc') {
          if (x.innerHTML.toLowerCase().localeCompare(y.innerHTML.toLowerCase()) < 0) {
            shouldSwitch = true
            break
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i])
        switching = true
        switchcount++
      } else {
        if (switchcount == 0 && dir == 'asc') {
          dir = 'desc'
          switching = true
        }
      }
    }
  }

  var sortTableByDate = function sortTableByDate (columnNumber) {
    try {
      var table, rows, switching, i, x, y, shouldSwitch, dir, date1, date2, switchcount = 0
      table = document.getElementById('ticketsTable')
      switching = true
      dir = 'asc'
      while (switching) {
        switching = false
        rows = table.rows
        for (i = 1; i < (rows.length - 1); i++) {
          shouldSwitch = false
          x = rows[i].getElementsByTagName('td')[columnNumber]
          y = rows[i + 1].getElementsByTagName('td')[columnNumber]
          date1 = new Date(convertToShortDate(x.innerHTML.toLowerCase()))
          date2 = new Date(convertToShortDate(y.innerHTML.toLowerCase()))
          if (dir == 'asc') {
            if (date1 - date2 > 0) {
              shouldSwitch = true
              break
            }
          } else if (dir == 'desc') {
            if (date1 - date2 < 0) {
              shouldSwitch = true
              break
            }
          }
        }
        if (shouldSwitch) {
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i])
          switching = true
          switchcount++
        } else {
          if (switchcount == 0 && dir == 'asc') {
            dir = 'desc'
            switching = true
          }
        }
      }
    } catch (error) {
      console.log(error.message)
    }
  }

  var sortTableByUrgency = function sortTableByUrgency (columnNumber) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0
    table = document.getElementById('ticketsTable')
    switching = true
    dir = 'desc'
    while (switching) {
      switching = false
      rows = table.rows
      for (i = 1; i < (rows.length - 1); i++) {
        shouldSwitch = false
        x = rows[i].getElementsByTagName('td')[columnNumber]
        y = rows[i + 1].getElementsByTagName('td')[columnNumber]
        var left = x.innerHTML.toLowerCase()
        var right = y.innerHTML.toLowerCase()
        if (left === 'critical') {
          left = 3
        } else if (left === 'high') {
          left = 2
        } else if (left === 'medium') {
          left = 1
        } else if (left === 'low') {
          left = 0
        }
        if (right === 'critical') {
          right = 3
        } else if (right === 'high') {
          right = 2
        } else if (right === 'medium') {
          right = 1
        } else if (right === 'low') {
          right = 0
        }
        if (dir == 'asc') {
          if (left > right) {
            shouldSwitch = true
            break
          }
        } else if (dir == 'desc') {
          if (left < right) {
            shouldSwitch = true
            break
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i])
        switching = true
        switchcount++
      } else {
        if (switchcount == 0 && dir == 'desc') {
          dir = 'asc'
          switching = true
        }
      }
    }
  }

  $scope.sortTableByUrgency = sortTableByUrgency

  $scope.searchTicket = function searchTicket (searchIsValid, input) {
    if (searchIsValid) {
      try {
        var filter, table, id, name, desiredDate, urgency, status, rows, i, tdId,
          tdName, tdDesiredDate, tdUrgency, tdStatus
        filter = input
        table = document.getElementById('ticketsTable')
        rows = table.getElementsByTagName('tr')
        for (i = 1; i < rows.length; i++) {
          tdId = rows[i].getElementsByTagName('td')[0]
          tdName = rows[i].getElementsByTagName('td')[1].getElementsByTagName('div')[0].getElementsByTagName('a')[0]
          tdDesiredDate = rows[i].getElementsByTagName('td')[2]
          tdUrgency = rows[i].getElementsByTagName('td')[3]
          tdStatus = rows[i].getElementsByTagName('td')[4]
          if (tdId) {
            id = tdId.innerHTML.indexOf(filter)
          }
          if (tdName) {
            name = tdName.innerHTML.indexOf(filter)
            $scope.test = 'tdName is true'
          }
          if (tdDesiredDate) {
            desiredDate = tdDesiredDate.innerHTML.indexOf(filter)
          }
          if (tdUrgency) {
            urgency = tdUrgency.innerHTML.indexOf(filter)
          }
          if (tdStatus) {
            status = tdStatus.innerHTML.indexOf(filter)
          }
          if (id > -1 || name > -1 || desiredDate > -1 || urgency > -1 || status > -1) {
            rows[i].style.display = ''
          } else {
            rows[i].style.display = 'none'
          }
        }
      } catch (error) {
        console.log(error.message)
      }
    }
  }

  var convertToShortDate = function (string) {
    var parts = string.split('/')
    return parts[1] + '/' + parts[0] + '/' + parts[2]
  }
}
])
