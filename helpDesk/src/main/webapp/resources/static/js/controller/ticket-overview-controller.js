app.controller('TicketOverviewController', ['$scope', '$window', 'TicketsService', 'AttachmentsService', 'HistoryService',
  'CommentService', 'UserService',
  function ($scope, $window, TicketsService, AttachmentsService, HistoryService, CommentService, UserService) {
    var history, comments, currentUser, ticket
    var parameters = $window.location.pathname.split('/')
    var ticketId = parameters[parameters.length - 1]
    $scope.ticketId = ticketId
    $scope.isHistoryTableAvailable = true
    $scope.isCommentsTableAvailable = false

    UserService.getCurrentUser()
      .then(
        function (response) {
          currentUser = response
        },
        function (errorResponse) {
          console.log(errorResponse.statusText)
        }
      )

    TicketsService.getTicketById(ticketId)
      .then(
        function (response) {
          ticket = response
          $scope.ticket = ticket
        },
        function (errorResponse) {
          console.log(errorResponse.statusText)
        }
      )

    AttachmentsService.getAttachmentsByTicketId(ticketId)
      .then(
        function (response) {
          $scope.attachments = response
          console.log(response)
        },
        function (errorResponse) {
          console.log(errorResponse.statusText)
        }
      )

    HistoryService.getHistoryById(ticketId)
      .then(
        function (response) {
          history = response
          var historyByDefault = []
          for (var i = response.length - 1; i > (response.length - 6); i--) {
            if (i < 0) {
              break
            }
            historyByDefault.push(response[i])
          }
          $scope.history = historyByDefault
          console.log(response)
        },
        function (errorResponse) {
          console.log(errorResponse.statusText)
        }
      )
    $scope.isEditAvailable = function () {
      if (ticket !== undefined && (ticket.state === 'DRAFT' ||
              ticket.state === 'DECLINED') && (currentUser.role === 'EMPLOYEE' || currentUser.role === 'MANAGER')) {
        return true
      }
      return false
    }

    $scope.isFeedbackAvailable = function () {
      if (ticket !== undefined && ticket.state === 'DONE' && (currentUser.email === ticket.owner.email ||
              currentUser.email === ticket.assignee.email)) {
        return true
      }
      return false
    }

    $scope.showHistory = function showHistory () {
      $scope.isHistoryTableAvailable = true
      $scope.isCommentsTableAvailable = false
    }

    $scope.showAllHistory = function () {
      $scope.history = history
    }

    var showComments = function () {
      console.log($scope.comments)
      CommentService.getCommentsByTicketId(ticketId)
        .then(
          function (response) {
            comments = response
            var commentsByDefault = []
            for (var i = response.length - 1; i > (response.length - 6); i--) {
              if (i < 0) {
                break
              }
              commentsByDefault.push(response[i])
            }
            $scope.comments = commentsByDefault
          },
          function (errorResponse) {
            console.log(errorResponse.statusText)
          })
      $scope.isHistoryTableAvailable = false
      $scope.isCommentsTableAvailable = true
    }
    $scope.showComments = showComments

    $scope.showAllComments = function () {
      $scope.comments = comments
    }

    $scope.addComment = function (isAvailable) {
      if (isAvailable) {
        CommentService.addComment(ticketId, $scope.commentText)
          .then(
            function (response) {
              showComments()
            },
            function (errorRespnse) {
              console.log(errorRespnse)
            })
      }
    }

    $scope.switchSortType = function (tableName, selectedSortType) {
      if (selectedSortType === 'date') {
        sortTableByDate(tableName, 0)
      }
      if (selectedSortType === 'user') {
        sortTableByName(tableName, 1)
      }

      if (selectedSortType === 'action') {
        sortTableByName(tableName, 2)
      }
      if (selectedSortType === 'description') {
        sortTableByName(tableName, 3)
      }
      if (selectedSortType === 'text') {
        sortTableByName(tableName, 2)
      }
    }

    var sortTableByDate = function sortTableByDate (tableName, columnNumber) {
      try {
        var table, rows, switching, i, x, y, shouldSwitch, dir, date1, date2, switchcount = 0
        table = document.getElementById(tableName)
        switching = true
        dir = 'asc'
        while (switching) {
          switching = false
          rows = table.rows
          for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false
            x = rows[i].getElementsByTagName('td')[columnNumber]
            y = rows[i + 1].getElementsByTagName('td')[columnNumber]
            date1 = new Date(convertToIsoDate(x.innerHTML.toLowerCase()))
            date2 = new Date(convertToIsoDate(y.innerHTML.toLowerCase()))
            if (dir == 'asc') {
              if (date1 - date2 > 0) {
                shouldSwitch = true
                break
              }
            } else if (dir == 'desc') {
              if (date1 - date2 < 0) {
                shouldSwitch = true
                break
              }
            }
          }
          if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i])
            switching = true
            switchcount++
          } else {
            if (switchcount == 0 && dir == 'asc') {
              dir = 'desc'
              switching = true
            }
          }
        }
      } catch (error) {
        console.log(error.message)
      }
    }

    var sortTableByName = function sortTableByName (tableName, columnNumber) {
      var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0
      table = document.getElementById(tableName)
      switching = true
      dir = 'asc'
      while (switching) {
        switching = false
        rows = table.rows
        for (i = 1; i < (rows.length - 1); i++) {
          shouldSwitch = false
          x = rows[i].getElementsByTagName('td')[columnNumber]
          y = rows[i + 1].getElementsByTagName('td')[columnNumber]
          if (dir == 'asc') {
            if (x.innerHTML.toLowerCase().localeCompare(y.innerHTML.toLowerCase()) > 0) {
              shouldSwitch = true
              break
            }
          } else if (dir == 'desc') {
            if (x.innerHTML.toLowerCase().localeCompare(y.innerHTML.toLowerCase()) < 0) {
              shouldSwitch = true
              break
            }
          }
        }
        if (shouldSwitch) {
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i])
          switching = true
          switchcount++
        } else {
          if (switchcount == 0 && dir == 'asc') {
            dir = 'desc'
            switching = true
          }
        }
      }
    }
    var convertToIsoDate = function (string) {
      var parts = string.split(' ')
      var date = parts[0].split('.')
      var time = parts[1].split(':')
      var d = date[0]
      var M = date[1]
      var y = date[2]
      var h = time[0]
      var m = time[1]
      var s = time[2]
      return y + '-' + M + '-' + d + 'T' + h + ':' + m + ':' + s + 'Z'
    }
  }
])
