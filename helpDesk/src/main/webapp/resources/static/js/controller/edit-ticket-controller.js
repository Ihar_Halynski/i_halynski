app.controller('EditTicketController', ['$scope', '$window', '$http', 'CategoryService',
  'UrgencyService', 'AttachmentsService', 'TicketsService',
  function ($scope, $window, $http, CategoryService, UrgencyService, AttachmentsService, TicketsService) {
    var parameters = $window.location.pathname.split('/')
    var ticketId = parameters[parameters.length - 1]
    $scope.ticketId = ticketId
    $scope.test = false
    $scope.isFileExtensionNotValid = false
    $scope.isFileSizeNotValid = false
    var ticket, categories, categoryId
    getCategories(function () {
      getTicketById(function () {
        console.log('CategoryID')
        console.log(categoryId)
        $scope.category = getCategoryById(categories, categoryId)
      })
    })

    function getCategoryById (categories, categoryId) {
      console.log('Categories Id:')
      for (i in categories) {
        console.log(categories[i].id)
        if (categories[i].id === categoryId) {
          return categories[i]
        }
      }
    }
    function getCategories (callback) {
      CategoryService.getCategories()
        .then(
          function (response) {
            categories = response
            $scope.categories = response
            console.log('Categories')
            console.log(response)
            callback()
          },
          function (error) {
            console.log(error)
          },
          function () {
            getTicketById()
          }
        )
    }

    function getTicketById (callback) {
      TicketsService.getTicketById(ticketId)
        .then(
          function (response) {
            ticket = response
            $scope.name = ticket.name
            console.log(ticket.category.id)
            console.log(Array.isArray(categories))
            categoryId = ticket.category.id
            console.log(getCategoryById(categories, ticket.category.id))
            $scope.selectedUrgency = ticket.urgency
            $scope.date = ticket.desiredResolutionDate
            if (ticket.description !== null) {
              $scope.description = ticket.description
            }
            callback()
          },
          function (errorResponse) {
            console.log(errorResponse.statusText)
          }
        )
    }

    UrgencyService.getUrgency()
      .then(
        function (response) {
          $scope.urgency = response
          $scope.selectedUrgency = response[0]
        },
        function (error) {
          console.log(error)
        }
      )

    var getAttachmentsByTicketId = function (ticketId) {
      AttachmentsService.getAttachmentsByTicketId(ticketId)
        .then(
          function (response) {
            $scope.attachments = response
            console.log(response)
          },
          function (errorResponse) {
            console.log(errorResponse.statusText)
          }
        )
    }
    getAttachmentsByTicketId(ticketId)

    $scope.deleteAttachment = function (attachmentId) {
      AttachmentsService.deleteAttachmentById(attachmentId)
        .then(
          function (response) {
            console.log(response)
            getAttachmentsByTicketId(ticketId)
          }
        )
    }

    $scope.checkFiles = function checkFiles (input) {
      $scope.file = input.files
      var file
      try {
        file = input.files[0]
        console.log(file.name)
        if (file.size < 1024 * 1024 * 5) {
          $scope.isFileSizeNotValid = false
          console.log('Valid')
        } else {
          $scope.isFileSizeNotValid = true
          console.log('NotValid')
        }
        var parts = file.name.split('.')
        var ext = parts[parts.length - 1].toLowerCase()
        switch (ext) {
          case 'pdf':
          case 'doc':
          case 'docx':
          case 'png':
          case 'jpeg':
          case 'jpg':
            $scope.isFileExtensionNotValid = false
            $scope.$apply()
            console.log('Extension is valid')
            return
        }
        console.log('Extension is not valid')
        $scope.isFileExtensionNotValid = true
        $scope.$apply()
      } catch (error) {
        console.log(error.message)
      }
    }

    function updateTicket (state) {
      console.log($scope.file)
      var name = $scope.name
      var selectedUrgency = $scope.selectedUrgency
      var description = $scope.description !== undefined ? $scope.description : ''
      var desiredDate = $scope.date !== undefined ? new Date($scope.date).getTime() + 86400000 : new Date().getTime()
      var comment = $scope.comment !== undefined ? $scope.comment : ''
      var data = {
        id: ticketId,
        state: state,
        categoryId: $scope.category.id,
        name: name,
        urgency: selectedUrgency,
        description: description,
        desiredResolutionDate: desiredDate,
        comment: comment
      }
      var fd = new FormData()
      angular.forEach($scope.file, function (value, key) {
        fd.append('files', value)
      })
      fd.append('ticketDto', JSON.stringify(data))
      var url = '/tickets/update-ticket'
      return $http({
        method: 'POST',
        url: url,
        headers: {
          'Content-Type': undefined
        },
        data: fd,
        transformRequest: function (data, headersGetterFunction) {
          return data
        }
      })
        .then(
          function (response) {
            $window.location.href = '/ticket-overview/' + ticket.id
          },
          function (error) {
            console.log(error)
            $window.location.href = '/ticket-overview/' + ticket.id
          }
        )
    }

    $scope.saveAsDraft = function (value) {
      if (value && !$scope.isFileExtensionNotValid && !$scope.isFileSizeNotValid) {
        updateTicket('DRAFT')
      }
    }
    $scope.saveNew = function (value) {
      if (value && !$scope.isFileExtensionNotValid && !$scope.isFileSizeNotValid) {
        updateTicket('NEW')
      }
    }
  }])
