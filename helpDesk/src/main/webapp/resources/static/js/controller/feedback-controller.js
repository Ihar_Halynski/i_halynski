app.controller('FeedbackController', ['$scope', '$window', '$http', 'UserService', 'FeedbackService', 'TicketsService',
  function ($scope, $window, $http, UserService, FeedbackService, TicketsService) {
    var parameters = $window.location.pathname.split('/')
    var ticketId = parameters[parameters.length - 1]
    $scope.ticketId = ticketId
    $scope.rating = 5

    var feedback
    FeedbackService.getFeedback(ticketId)
      .then(
        function (response) {
          feedback = response
          console.log('Feedback:')
          console.log(response)
          if (feedback === '') {
            $scope.isFeedbackEditAvailable = true
            $scope.isFeedbackEmpty = true
          } else {
            $scope.isFeedbackEditAvailable = false
            $scope.isFeedbackEmpty = false
            $scope.rating = feedback.rate
            $('#input-rating').rating('refresh', {
              displayOnly: true,
            })
            $('#input-rating').rating('update', feedback.rate)
            $scope.comment = feedback.text
          }
        }
      )

    UserService.getCurrentUser()
      .then(
        function (response) {
          $scope.currentUser = response
        }
      )

    TicketsService.getTicketById(ticketId)
      .then(
        function (response) {
          $scope.ticket = response
        },
        function (errorResponse) {
          console.log(errorResponse.statusText)
        }
      )

    $('#input-rating').on('rating:change', function (event, value, caption) {
      $scope.rating = value
      $scope.$apply()
    })

    $scope.addFeedback = function (isValid, rating, comment) {
      // $scope.$apply()
      console.log('Rating: ' + $scope.rating)
      if ($scope.rating > 0 && isValid) {
        FeedbackService.leaveFeedback($scope.ticketId, rating, comment)
          .then(
            function (reasponse) {
              console.log(reasponse)
              $window.location.href = '/ticket-overview/' + ticketId
            }
          )
      }
    }
  }
])
