app.service('UserService', ['$http', function ($http) {
  this.getCurrentUser = function () {
    return $http.get('/users/current')
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }
}])
