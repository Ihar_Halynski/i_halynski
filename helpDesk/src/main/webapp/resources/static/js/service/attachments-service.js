app.service('AttachmentsService', ['$http', function ($http) {
  this.getAttachmentsByTicketId = function (ticketId) {
    return $http.get('/attachments/get-all-for-ticket/' + ticketId)
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }

  // this.getAttachmentById = function (id) {
  //   return $http.get('/attachments/get-attachment/' + id)
  //     .then(
  //       function (response) {
  //         return response.data
  //       },
  //       function (errResponse) {
  //         return errResponse.statusText
  //       }
  //     )
  // }

  this.deleteAttachmentById = function (id) {
    return $http.get('/attachments/delete/' + id)
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }
}])
