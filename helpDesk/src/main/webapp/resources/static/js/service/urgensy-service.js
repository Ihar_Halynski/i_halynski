app.service('UrgencyService', ['$http', function ($http) {
  this.getUrgency = function () {
    return $http.get('/urgency')
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }
}])
