app.service('TicketsService', ['$http', function ($http) {
  this.getAllTickets = function () {
    return $http.get('/tickets/get-all')
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }

  this.getTicketById = function (id) {
    return $http.get('/tickets/get-ticket/' + id)
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }

  this.approve = function (id) {
    return $http.get('/tickets/approve/' + id)
      .then(
        function (response) {
          return true
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }

  this.submit = function (id) {
    return $http.get('/tickets/submit/' + id)
      .then(
        function (response) {
          return true
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }

  this.cancel = function (id) {
    return $http.get('/tickets/cancel/' + id)
      .then(
        function (response) {
          return true
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }

  this.decline = function (id) {
    return $http.get('/tickets/decline/' + id)
      .then(
        function (response) {
          return true
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }

  this.assign = function (id) {
    return $http.get('/tickets/assign/' + id)
      .then(
        function (response) {
          return true
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }

  this.done = function (id) {
    return $http.get('/tickets/done/' + id)
      .then(
        function (response) {
          return true
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }
}])
