app.service('CommentService', ['$http', function ($http) {
  this.getCommentsByTicketId = function (id) {
    return $http.get('/comments/get-comments/' + id)
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }

  this.addComment = function (ticketId, text) {
    var fd = new FormData()
    fd.append('ticketId', ticketId)
    fd.append('text', text)
    var config = { headers: {
      'Content-Type': undefined
    }}
    return $http.post('/comments/add-comment', fd, config)
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }
}])
