app.service('HistoryService', ['$http', function ($http) {
  this.getHistoryById = function (id) {
    return $http.get('/history/get/' + id)
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }
}])
