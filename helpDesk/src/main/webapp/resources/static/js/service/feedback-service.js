app.service('FeedbackService', ['$http', function ($http) {
  this.leaveFeedback = function (ticketId, rating, text) {
    var fd = new FormData()
    fd.append('ticketId', ticketId)
    fd.append('rating', rating)
    text = text !== undefined ? text : ''
    fd.append('text', text)
    var config = { headers: {
      'Content-Type': undefined
    }}
    return $http.post('/feedback/leave', fd, config)
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }

  this.getFeedback = function (ticketId) {
    return $http.get('/feedback/get/' + ticketId)
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }
}])
