app.service('CategoryService', ['$http', function ($http) {
  this.getCategories = function () {
    return $http.get('/categories')
      .then(
        function (response) {
          return response.data
        },
        function (errResponse) {
          return errResponse.statusText
        }
      )
  }
}])
