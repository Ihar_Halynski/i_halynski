INSERT INTO user (first_name, last_name, role_id, email, password) VALUES (
   'Thomas', 'Anderson', 0, 'user1_mogilev@yopmail.com', '$2a$04$sG4KjFmvdPIy9V8E5sH1QuiNPB44apYSci.tNlBdOGF00XW7Im3pm'
);
INSERT INTO user ( first_name, last_name, role_id, email, password) VALUES (
  'Pavel', 'Durov', 0, 'user2_mogilev@yopmail.com', '$2a$04$sG4KjFmvdPIy9V8E5sH1QuiNPB44apYSci.tNlBdOGF00XW7Im3pm'
);
INSERT INTO user ( first_name, last_name, role_id, email, password) VALUES (
 'Steve', 'Wozniak', 1, 'manager1_mogilev@yopmail.com', '$2a$04$sG4KjFmvdPIy9V8E5sH1QuiNPB44apYSci.tNlBdOGF00XW7Im3pm'
);
INSERT INTO user ( first_name, last_name, role_id, email, password) VALUES (
   'Mark', 'Zuckerberg', 1, 'manager2_mogilev@yopmail.com', '$2a$04$sG4KjFmvdPIy9V8E5sH1QuiNPB44apYSci.tNlBdOGF00XW7Im3pm'
);
INSERT INTO user ( first_name, last_name, role_id, email, password) VALUES (
  'James', 'Gosling', 2, 'engineer1_mogilev@yopmail.com', '$2a$04$sG4KjFmvdPIy9V8E5sH1QuiNPB44apYSci.tNlBdOGF00XW7Im3pm'
);
INSERT INTO user ( first_name, last_name, role_id, email, password) VALUES (
  'Herbert', 'Schildt', 2, 'engineer2_mogilev@yopmail.com', '$2a$04$sG4KjFmvdPIy9V8E5sH1QuiNPB44apYSci.tNlBdOGF00XW7Im3pm'
);

INSERT INTO category (name) VALUES ('Application & Services');

INSERT INTO category (name) VALUES ('Benefits & Paper Work');

INSERT INTO category (name) VALUES ('Hardware & Software');

INSERT INTO category (name) VALUES ('People Management');

INSERT INTO category (name) VALUES ('Security & Access');

INSERT INTO category (name) VALUES ('Workplaces & Facilities');