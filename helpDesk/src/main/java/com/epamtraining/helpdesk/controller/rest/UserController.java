package com.epamtraining.helpdesk.controller.rest;

import com.epamtraining.helpdesk.model.User;
import com.epamtraining.helpdesk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public User getCurrentUser(Authentication authentication){
        User currentUser = null;
        if (authentication.getPrincipal() != null) {
            currentUser = userService.loadUserByUsername(authentication.getName());
        }
        return currentUser;
    }

}
