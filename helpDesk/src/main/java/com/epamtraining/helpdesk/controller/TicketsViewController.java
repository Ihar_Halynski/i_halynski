package com.epamtraining.helpdesk.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TicketsViewController {

    @RequestMapping(value = {"/home", "/"}, method = RequestMethod.GET)
    public String getHomePage() {
        return "tickets";
    }

    @RequestMapping(value = "/tickets", method = RequestMethod.GET)
    public String getTicketsPage(){
        return "tickets";
    }

    @RequestMapping(value = "/createnewticket", method = RequestMethod.GET)
    public String getCreateNewTicketPage() {return "create-ticket";}

    @RequestMapping(value = "/ticket-overview/{ticketId}", method = RequestMethod.GET)
    public String getTicketOverview(@PathVariable int ticketId){
        return "ticket-overview";
    }

    @RequestMapping(value = "/edit-ticket/{ticketId}", method = RequestMethod.GET)
    public String getTicketEdition(@PathVariable int ticketId){
        return "ticket-edition";
    }

    @RequestMapping(value = "/leave-feedback/{ticketId}", method = RequestMethod.GET)
    public String getFeedbackPage(@PathVariable int ticketId){
        return "feedback";
    }
}
