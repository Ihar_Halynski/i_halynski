package com.epamtraining.helpdesk.controller.rest;

import com.epamtraining.helpdesk.dto.AttachmentDto;
import com.epamtraining.helpdesk.model.Attachment;
import com.epamtraining.helpdesk.service.AttachmentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.util.List;

@RestController
@RequestMapping(value = "attachments/")
public class AttachmentController {

    @Autowired
    private Logger logger;

    @Autowired
    private AttachmentService attachmentService;

    @RequestMapping(value = "get-all-for-ticket/{ticketId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AttachmentDto> getAllByTicketId(@PathVariable int ticketId) {
        return attachmentService.getAttachmentsDtoByTicketId(ticketId);
    }

    @RequestMapping(value = "get-attachment/{id}", method = RequestMethod.GET)
    public void getById(HttpServletResponse response, @PathVariable int id) {
        InputStream is = null;
        try {
            Attachment attachment = attachmentService.loadAttachmentById(id);
            String name = attachment.getName();
            is = new ByteArrayInputStream(attachment.getBlob());
            String mimeType = URLConnection.guessContentTypeFromName(name);
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }
            response.setContentType(mimeType);
            response.setHeader("Content-Disposition", String.format("inline; filename=\"" + name + "\""));
            response.setContentLength(attachment.getBlob().length);
            InputStream inputStream = new BufferedInputStream(is);
            org.apache.commons.io.IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            logger.info("Error writing file to output stream");
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    logger.info(ex.getMessage());
                }

            }
        }
    }

    @RequestMapping(value = "delete/{attachmentId}", method = RequestMethod.GET)
    public int deleteAttachmentById(@PathVariable int attachmentId) {
        return attachmentService.delete(attachmentId);
    }


}
