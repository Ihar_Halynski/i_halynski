package com.epamtraining.helpdesk.controller.rest;

import com.epamtraining.helpdesk.model.Category;
import com.epamtraining.helpdesk.service.CategoryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    private Logger logger;

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Category> loadCategories(){
        List<Category>  result = categoryService.loadCategories();
        return result;
    }
}
