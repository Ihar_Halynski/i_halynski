package com.epamtraining.helpdesk.controller.rest;

import com.epamtraining.helpdesk.enums.UserRole;
import com.epamtraining.helpdesk.model.Ticket;
import com.epamtraining.helpdesk.model.User;
import com.epamtraining.helpdesk.service.TicketService;
import com.epamtraining.helpdesk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.epamtraining.helpdesk.excetpions.InvalidDataException;

import java.util.List;

@RestController
@RequestMapping(value = "tickets")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/create-new", method = RequestMethod.POST)
    public void createNewTicket(@RequestParam("files") MultipartFile[] files,
                                Authentication authentication,
                                @RequestParam(value = "ticketDto") String ticketDtoJson) throws InvalidDataException {
        ticketService.saveNewTicket(ticketDtoJson, files, authentication.getName());
    }

    @RequestMapping(value = "/update-ticket", method = RequestMethod.POST)
    public void updateTicket(@RequestParam("files") MultipartFile[] files,
                                Authentication authentication,
                                @RequestParam(value = "ticketDto") String ticketDtoJson) throws InvalidDataException {
        ticketService.update(ticketDtoJson, files, authentication.getName());
    }

    @RequestMapping(value = "/get-all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Ticket> getAllTickets() {
        return ticketService.loadAllTickets();
    }

    @RequestMapping(value = "/get-ticket/{ticketId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Ticket getTicketById(@PathVariable int ticketId ) {
        return ticketService.loadTicketById(ticketId);
    }

    @RequestMapping(value = "/submit/{ticketId}", method = RequestMethod.GET)
    public void submit(@PathVariable int ticketId, Authentication authentication) {
        ticketService.submit(ticketId, authentication.getName());
    }

    @RequestMapping(value = "/approve/{ticketId}", method = RequestMethod.GET)
    public void approve(@PathVariable int ticketId, Authentication authentication) {
        ticketService.approve(ticketId, authentication.getName());
    }

    @RequestMapping(value = "/cancel/{ticketId}", method = RequestMethod.GET)
    public void cancel(@PathVariable int ticketId, Authentication authentication) {
        User user = userService.loadUserByUsername(authentication.getName());
        UserRole userRole = user.getRole();
        if (userRole == UserRole.EMPLOYEE) {
            ticketService.cancelForEmployee(ticketId, authentication.getName());
        } else if (userRole == UserRole.MANAGER) {
            ticketService.cancelForManager(ticketId, authentication.getName());
        } else if (userRole == UserRole.ENGINEER) {
            ticketService.cancelForEngineer(ticketId, authentication.getName());
        }
    }

    @RequestMapping(value = "/decline/{ticketId}", method = RequestMethod.GET)
    public void decline(@PathVariable int ticketId, Authentication authentication ) {
        ticketService.decline(ticketId, authentication.getName());
    }

    @RequestMapping(value = "/assign/{ticketId}", method = RequestMethod.GET)
    public void assign(@PathVariable int ticketId, Authentication authentication ) {
        ticketService.assign(ticketId, authentication.getName());
    }

    @RequestMapping(value = "/done/{ticketId}", method = RequestMethod.GET)
    public void done(@PathVariable int ticketId, Authentication authentication ) {
        ticketService.done(ticketId, authentication.getName());
    }
}
