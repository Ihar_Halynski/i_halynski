package com.epamtraining.helpdesk.controller.rest;

import com.epamtraining.helpdesk.enums.Urgency;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Arrays;
import java.util.List;

@RestController
public class UrgencyController {

    @RequestMapping(value = "/urgency", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Urgency> getUrgency() {
                return Arrays.asList(Urgency.values());
    }
}
