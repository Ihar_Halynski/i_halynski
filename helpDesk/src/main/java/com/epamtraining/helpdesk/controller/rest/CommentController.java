package com.epamtraining.helpdesk.controller.rest;

import com.epamtraining.helpdesk.model.Comment;
import com.epamtraining.helpdesk.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "comments")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/get-comments/{ticketId}")
    public List<Comment> getCommentsByTicketId(@PathVariable int ticketId){
        return commentService.loadCommentsByTicketId(ticketId);
    }

    @RequestMapping(value = "/add-comment", method = RequestMethod.POST)
    public void addComment(@RequestParam("ticketId") int ticketId,
                           @RequestParam("text") String text, Authentication authentication){
        commentService.save(ticketId, authentication.getName(), text);
    }
}
