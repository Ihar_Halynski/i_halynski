package com.epamtraining.helpdesk.controller.rest;

import com.epamtraining.helpdesk.excetpions.InvalidDataException;
import com.epamtraining.helpdesk.model.Feedback;
import com.epamtraining.helpdesk.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "feedback")
public class FeedbackController {

    @Autowired
    private FeedbackService feedbackService;

    @RequestMapping(value = "/leave", method = RequestMethod.POST)
    public void leaveFedback(@RequestParam("ticketId") int ticketId, @RequestParam("rating") int rating,
                             @RequestParam("text") String text,
                             Authentication authentication) throws InvalidDataException {
        Feedback feedback = new Feedback();
        feedback.setRate(rating);
        feedback.setText(text);
        feedbackService.save(feedback, authentication.getName(), ticketId);
    }

    @RequestMapping(value = "/get/{ticketId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Feedback getFeedbackByTicketId(@PathVariable int ticketId,
                             Authentication authentication) throws InvalidDataException {
       return feedbackService.loadFeedbackByTicketId(ticketId, authentication.getName());
    }
}
