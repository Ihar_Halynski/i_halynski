package com.epamtraining.helpdesk.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLoginPage(Model model) {
        return "login";
    }

    @RequestMapping(value = "/login-error", method = RequestMethod.GET)
    public String getLoginErrorPage(Model model) {
        model.addAttribute("message", "Please make sure you are\n" +
                "using a valid email or password");
        return "error";
    }
}
