package com.epamtraining.helpdesk.controller.rest;

import com.epamtraining.helpdesk.model.HistoryItem;
import com.epamtraining.helpdesk.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping(value = "history")
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    @RequestMapping(value = "/get/{ticketId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<HistoryItem> getHistoryByTicketId(@PathVariable int ticketId){
        return historyService.loadHistoryByTicketId(ticketId);
    }
}
