package com.epamtraining.helpdesk.converters;

import com.epamtraining.helpdesk.dto.TicketDto;
import com.epamtraining.helpdesk.model.Ticket;
import org.springframework.stereotype.Component;
import java.sql.Date;
import java.util.Optional;

@Component
public class TicketConverter {

    public Optional<Ticket> convertToEntity(final TicketDto ticketDto) {
        Ticket ticket = null;
        if (ticketDto != null) {
            ticket = new Ticket();
            if (ticketDto.getId() == null) {
                ticket.setId(0);
            } else {
                ticket.setId(ticketDto.getId());
            }
            ticket.setName(ticketDto.getName());
            ticket.setCategory(ticketDto.getCategory());
            ticket.setDescription(ticketDto.getDescription());
            ticket.setDesiredResolutionDate(ticketDto.getDesiredResolutionDate());
            if (ticket.getDesiredResolutionDate() == null) {
                ticket.setDesiredResolutionDate(new Date(new java.util.Date().getTime()));
            }
            ticket.setOwner(ticketDto.getOwner());
            ticket.setUrgency(ticketDto.getUrgency());
            ticket.setState(ticketDto.getState());
        }
        return Optional.of(ticket);
    }
}
