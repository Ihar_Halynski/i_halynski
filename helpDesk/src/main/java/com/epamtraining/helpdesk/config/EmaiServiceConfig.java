package com.epamtraining.helpdesk.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import java.util.Properties;

@Configuration
@PropertySource("classpath:/email/email-config.properties")
@ComponentScan({"com.epamtraining.helpdesk"})
public class EmaiServiceConfig {
    @Value("${email.host}")
    private String host;

    @Value("${email.port}")
    private String port;

    @Value("${email.protocol}")
    private String protocol;

    @Value("${email.userName}")
    private String userName;

    @Value("${email.password}")
    private String password;

    @Value("${email.sender}")
    private String sender;

    @Value("${email.ticketOverviewPatternAddress}")
    private String ticketOverviewPatternAddress;

    @Bean
    public JavaMailSender getJavaMailSender() {
        final JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setProtocol(protocol);
        mailSender.setPort(Integer.parseInt(port));
        mailSender.setUsername(userName);
        mailSender.setPassword(password);
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.quitwait", "false");
        return mailSender;
    }

    @Bean(name = "hostAddress")
    public String getHostAddress(){
        return sender;
    }

    @Bean(name = "ticketOverviewPatternAddress")
    public String getTicketOverviewPatternAddress(){
        return ticketOverviewPatternAddress;
    }


}
