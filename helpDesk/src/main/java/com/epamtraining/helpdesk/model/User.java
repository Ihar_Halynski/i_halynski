package com.epamtraining.helpdesk.model;

import com.epamtraining.helpdesk.enums.UserRole;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

    @Entity
    @Table(name = "user")
    public class User {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @NotNull
        @Column(name = "id")
        private int id;

        @Column(name = "first_name")
        @NotNull
        @Size(max = 25)
        private String firstName;

        @Column(name = "last_name")
        @NotNull
        @Size(max = 25)
        private String lastName;

        @Column(name = "role_id")
        @NotNull
        @Enumerated(EnumType.ORDINAL)
        private UserRole role;

        @Column(name = "email", unique = true)
        @NotNull
        @Size(max = 100)
        @Email
        private String email;

        @Column(name = "password")
        @NotNull
        private String password;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public UserRole getRole() {
            return role;
        }

        public void setRole(UserRole role) {
            this.role = role;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
}
