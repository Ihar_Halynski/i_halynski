package com.epamtraining.helpdesk.repository;

import com.epamtraining.helpdesk.model.HistoryItem;
import java.util.List;

public interface HistoryRepository {

    void save(HistoryItem historyItem);

    List<HistoryItem> loadItemsByTicketId(int id);

}
