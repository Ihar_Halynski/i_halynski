package com.epamtraining.helpdesk.repository.implementations;

import com.epamtraining.helpdesk.model.Feedback;
import com.epamtraining.helpdesk.repository.FeedbackRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class FeedbackRepositoryImpl implements FeedbackRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void save(Feedback feedback) {
        sessionFactory.getCurrentSession().save(feedback);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Feedback loadFeedbackByTicketId(int ticketId) {
        String hql = "from Feedback where ticket.id = :ticketId";
        Feedback feedback = (Feedback) sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("ticketId", ticketId)
            .uniqueResult();
        return feedback;
    }
}
