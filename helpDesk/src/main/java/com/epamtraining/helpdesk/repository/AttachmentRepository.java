package com.epamtraining.helpdesk.repository;

import com.epamtraining.helpdesk.model.Attachment;
import java.util.List;

public interface AttachmentRepository {

    void save(Attachment attachment);

    List<Attachment> loadAttachmentsByTicketId(int ticketId);

    Attachment loadAttachmentById(int id);

    int deleteAttachmentById(int id);
}
