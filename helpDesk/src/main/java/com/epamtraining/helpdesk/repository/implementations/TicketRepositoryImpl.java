package com.epamtraining.helpdesk.repository.implementations;

import com.epamtraining.helpdesk.enums.State;
import com.epamtraining.helpdesk.model.Ticket;
import com.epamtraining.helpdesk.repository.TicketRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public class TicketRepositoryImpl implements TicketRepository {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void save(Ticket ticket) {
        sessionFactory.getCurrentSession().save(ticket);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public int update(Ticket ticket) {
        String hql = "update Ticket as t set state = :state," +
            "name = :name," +
            "category = :category," +
            "urgency = :urgency," +
            "desiredResolutionDate = :desiredResolutionDate," +
            "description = :description" +
            " where id = :id ";
        return sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("state", ticket.getState())
            .setParameter("id", ticket.getId())
            .setParameter("category", ticket.getCategory())
            .setParameter("urgency", ticket.getUrgency())
            .setParameter("desiredResolutionDate", ticket.getDesiredResolutionDate())
            .setParameter("name", ticket.getName())
            .setParameter("description", ticket.getDescription())
            .executeUpdate();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Ticket> loadAllTickets() {
        String hql = "from Ticket";
        List<Ticket> result = sessionFactory.getCurrentSession().createQuery(hql).list();
        return result;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public int updateStateToApproved(int ticketId) {
        String hql = "update Ticket set state = :state where id = :id ";
        return sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("state", State.APPROVED)
            .setParameter("id", ticketId)
            .executeUpdate();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public int updateStateToDecline(int ticketId) {
        String hql = "update Ticket set state = :state where id = :id ";
        return sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("state", State.DECLINED)
            .setParameter("id", ticketId)
            .executeUpdate();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public int updateStateToCancel(int ticketId) {
        String hql = "update Ticket set state = :state where id = :id ";
        return sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("state", State.CANCELED)
            .setParameter("id", ticketId)
            .executeUpdate();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public int updateStateToSubmit(int ticketId) {
        String hql = "update Ticket set state = :state where id = :id ";
        return sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("state", State.NEW)
            .setParameter("id", ticketId)
            .executeUpdate();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public int updateStateToInProgress(int ticketId) {
        String hql = "update Ticket set state = :state where id = :id ";
        return sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("state", State.IN_PROGRESS)
            .setParameter("id", ticketId)
            .executeUpdate();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public int updateStateToDone(int ticketId) {
        String hql = "update Ticket set state = :state where id = :id ";
        return sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("state", State.DONE)
            .setParameter("id", ticketId)
            .executeUpdate();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Ticket loadTicketById(int id) {
        String hql = "from Ticket where id = :id";
        return (Ticket) sessionFactory.getCurrentSession().createQuery(hql)
             .setParameter("id", id)
            .uniqueResult();

    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public int updateAssignee(String userEmail, int ticketId) {
        String hql = "update Ticket set assignee = (select u from User as u where u.email = :email) where id = :id";
        return sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("email", userEmail)
            .setParameter("id", ticketId)
            .executeUpdate();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public int updateApprover(String userEmail, int ticketId) {
        String hql = "update Ticket set approver = (select u from User as u where u.email = :email) where id = :id";
        return sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("email", userEmail)
            .setParameter("id", ticketId)
            .executeUpdate();
    }
}
