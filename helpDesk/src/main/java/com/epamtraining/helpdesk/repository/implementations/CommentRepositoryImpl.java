package com.epamtraining.helpdesk.repository.implementations;

import com.epamtraining.helpdesk.model.Comment;
import com.epamtraining.helpdesk.repository.CommentRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public class CommentRepositoryImpl implements CommentRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void save(Comment comment) {
        sessionFactory.getCurrentSession().save(comment);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Comment> loadCommentsByTicketId(int ticketId) {
        String hqlQuery = "from Comment where ticket.id = :ticketId";
        return sessionFactory.getCurrentSession()
            .createQuery(hqlQuery)
            .setParameter("ticketId", ticketId)
            .list();
    }
}
