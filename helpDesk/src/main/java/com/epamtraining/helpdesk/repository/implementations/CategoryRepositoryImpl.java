package com.epamtraining.helpdesk.repository.implementations;

import com.epamtraining.helpdesk.model.Category;
import com.epamtraining.helpdesk.repository.CategoryRepository;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private Logger logger;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Category> loadCategories() {
        String hql = "from Category";
        List<Category> result = sessionFactory.getCurrentSession().createQuery(hql).list();
        return result;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Category loadCategoryById(int id) {
        Category category;
        String hql = "from Category where id = :id";
        category = (Category) sessionFactory.getCurrentSession()
                .createQuery(hql)
                .setParameter("id", id)
                .uniqueResult();
        return category;

    }
}
