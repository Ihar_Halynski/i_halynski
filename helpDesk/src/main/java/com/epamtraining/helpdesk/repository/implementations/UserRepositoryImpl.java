package com.epamtraining.helpdesk.repository.implementations;

import com.epamtraining.helpdesk.enums.UserRole;
import com.epamtraining.helpdesk.model.User;
import com.epamtraining.helpdesk.repository.UserRepository;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private Logger logger;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public User loadUserByUsername(String username) {
        String hql = "from User where email= :email";
        User user = (User) sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("email", username)
            .uniqueResult();
        logger.info("Loaded User Name: " + user.getFirstName() + " " + user.getLastName());
        return user;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<User> loadAllManagers() {
        List<User> managers;
        String hql = "from User u where u.role = :role";
        return sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("role", UserRole.MANAGER)
            .list();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<User> loadAllEngineers() {
        List<User> engineers;
        String hql = "from User u where u.role = :role";
        engineers = sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("role", UserRole.ENGINEER)
            .list();
        return engineers;
    }
}
    
