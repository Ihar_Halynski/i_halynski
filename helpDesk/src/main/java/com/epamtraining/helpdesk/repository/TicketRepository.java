package com.epamtraining.helpdesk.repository;

import com.epamtraining.helpdesk.model.Ticket;
import java.util.List;

public interface TicketRepository {

    void save(Ticket ticket);

    List<Ticket> loadAllTickets();

    Ticket loadTicketById(int id);

    int update(Ticket ticket);

    int updateStateToApproved(int ticketId);

    int updateStateToDecline(int ticketId);

    int updateStateToCancel(int ticketId);

    int updateStateToSubmit(int ticketId);

    int updateStateToInProgress(int ticketId);

    int updateStateToDone(int ticketId);

    int updateAssignee(String userEmail, int ticketId);

    int updateApprover(String userEmail, int ticketId);
}
