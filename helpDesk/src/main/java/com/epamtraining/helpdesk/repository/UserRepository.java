package com.epamtraining.helpdesk.repository;

import com.epamtraining.helpdesk.model.User;
import java.util.List;

public interface UserRepository {

    User loadUserByUsername(String username);

    List<User> loadAllManagers();

    List<User> loadAllEngineers();
}
