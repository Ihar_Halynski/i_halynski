package com.epamtraining.helpdesk.repository;

import com.epamtraining.helpdesk.model.Feedback;

public interface FeedbackRepository {

    void save(Feedback feedback);

    Feedback loadFeedbackByTicketId(int ticketId);
}
