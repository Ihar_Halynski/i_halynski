package com.epamtraining.helpdesk.repository;

import com.epamtraining.helpdesk.model.Category;
import java.util.List;

public interface CategoryRepository {

    List<Category> loadCategories();

    Category loadCategoryById(int id);
}
