package com.epamtraining.helpdesk.repository;

import com.epamtraining.helpdesk.model.Comment;
import java.util.List;

public interface CommentRepository {

    void save(Comment comment);

    List<Comment> loadCommentsByTicketId(int ticketId);
}
