package com.epamtraining.helpdesk.repository.implementations;

import com.epamtraining.helpdesk.model.Attachment;
import com.epamtraining.helpdesk.repository.AttachmentRepository;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class AttachmentRepositoryImpl implements AttachmentRepository {

    @Autowired
    private Logger logger;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void save(Attachment attachment) {
        sessionFactory.getCurrentSession().save(attachment);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Attachment> loadAttachmentsByTicketId(int ticketId) {
        String hql = "from Attachment where ticket.id = :ticketId";
       List<Attachment> result = sessionFactory.getCurrentSession().
            createQuery(hql).
            setParameter("ticketId", ticketId).
            list();
       return result;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Attachment loadAttachmentById(int id) {
        String hql = "from Attachment where id = :id";
        Attachment result = (Attachment) sessionFactory.getCurrentSession().
            createQuery(hql).
            setParameter("id", id).
            uniqueResult();
        return result;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public int deleteAttachmentById(int id) {
        String hql = "delete from Attachment where id = :id";
        return sessionFactory.getCurrentSession().
            createQuery(hql)
            .setParameter("id", id)
            .executeUpdate();
    }
}
