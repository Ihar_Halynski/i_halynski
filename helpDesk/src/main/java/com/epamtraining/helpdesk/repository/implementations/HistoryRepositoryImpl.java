package com.epamtraining.helpdesk.repository.implementations;

import com.epamtraining.helpdesk.model.HistoryItem;
import com.epamtraining.helpdesk.repository.HistoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public class HistoryRepositoryImpl implements HistoryRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void save(HistoryItem historyItem) {
        sessionFactory.getCurrentSession().save(historyItem);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<HistoryItem> loadItemsByTicketId(int id) {
        String hqlQuery = "from HistoryItem where ticket.id = :ticketId";
        return sessionFactory.getCurrentSession()
            .createQuery(hqlQuery)
            .setParameter("ticketId", id)
            .list();
    }
}
