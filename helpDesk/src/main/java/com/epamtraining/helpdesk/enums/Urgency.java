package com.epamtraining.helpdesk.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Urgency {
    CRITICAL("Critical"),
    HIGH("High"),
    MEDIUM("Medium"),
    LOW("Low");

    private String name;

    Urgency(String name){
        this.name = name;
    }

    @JsonValue
    public String getName() {
        return name;
    }
}
