package com.epamtraining.helpdesk.enums;

public enum UserRole {
    EMPLOYEE,
    MANAGER,
    ENGINEER;

    UserRole(){

    }

}
