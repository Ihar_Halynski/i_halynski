package com.epamtraining.helpdesk.service;

import com.epamtraining.helpdesk.excetpions.InvalidDataException;
import com.epamtraining.helpdesk.model.Feedback;

public interface FeedbackService {
    void save(Feedback feedback, String username, int ticketId);

    Feedback loadFeedbackByTicketId(int ticketId, String userName) throws InvalidDataException;

}
