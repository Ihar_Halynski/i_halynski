package com.epamtraining.helpdesk.service;

import com.epamtraining.helpdesk.model.User;

import java.util.List;

public interface UserService {

    User loadUserByUsername(String username);

    List<User> loadAllManagers();

    List<User> loadAllEngineers();
}
