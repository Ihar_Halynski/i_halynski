package com.epamtraining.helpdesk.service.implementations;

import com.epamtraining.helpdesk.dto.AttachmentDto;
import com.epamtraining.helpdesk.model.Attachment;
import com.epamtraining.helpdesk.model.Ticket;
import com.epamtraining.helpdesk.model.User;
import com.epamtraining.helpdesk.repository.AttachmentRepository;
import com.epamtraining.helpdesk.repository.TicketRepository;
import com.epamtraining.helpdesk.service.AttachmentService;
import com.epamtraining.helpdesk.service.HistoryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    @Autowired
    private AttachmentRepository attachmentRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private Logger logger;

    @Override
    public void save(Attachment attachment) {
        attachmentRepository.save(attachment);
    }

    @Override
    public List<Attachment> loadAttachmentsByTicketId(int ticketId) {
        return attachmentRepository.loadAttachmentsByTicketId(ticketId);
    }

    public List<AttachmentDto> getAttachmentsDtoByTicketId(int ticketId) {
        List<AttachmentDto> attachmentsDto = new ArrayList<>();
        List<Attachment> attachments = loadAttachmentsByTicketId(ticketId);
        for (Attachment attachment : attachments) {
            attachmentsDto.add(AttachmentDto.AttachmentDtoBuilder.aAttachmentDto()
                .withId(attachment.getId())
                .withName(attachment.getName())
                .withTicket(attachment.getTicket())
                .withExtension(attachment.getExtension())
                .build());
        }
        return attachmentsDto;
    }

    @Override
    public Attachment loadAttachmentById(int id) {
        return attachmentRepository.loadAttachmentById(id);
    }

    @Override
    public void saveAttachments(MultipartFile[] attachments, Ticket ticket, User user) {
        for (MultipartFile file : attachments) {
            if (file.isEmpty()) {
                continue;
            }
            try {
                byte[] bytes = file.getBytes();
                Attachment attachment = new Attachment();
                attachment.setName(file.getOriginalFilename());
                attachment.setBlob(bytes);
                attachment.setExtension(file.getContentType());
                attachment.setTicket(ticketRepository.loadTicketById(ticket.getId()));
                this.save(attachment);
                historyService.createItemForAttachingFile(ticket, user, file.getName());
            } catch (IOException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }

    }

    @Override
    public int delete(int attachmentId) {
        return attachmentRepository.deleteAttachmentById(attachmentId);
    }
}
