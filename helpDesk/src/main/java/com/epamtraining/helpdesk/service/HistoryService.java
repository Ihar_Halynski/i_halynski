package com.epamtraining.helpdesk.service;

import com.epamtraining.helpdesk.enums.State;
import com.epamtraining.helpdesk.model.HistoryItem;
import com.epamtraining.helpdesk.model.Ticket;
import com.epamtraining.helpdesk.model.User;
import java.util.List;

public interface HistoryService {

    List<HistoryItem> loadHistoryByTicketId(int ticketId);

    void save(HistoryItem historyItem);

    boolean createItemForCreation(Ticket ticket, User user);

    boolean createItemForChangingStatus(Ticket ticket, User user, State from, State to);

    boolean createItemForAttachingFile(Ticket ticket, User user, String filename);
}
