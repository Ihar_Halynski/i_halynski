package com.epamtraining.helpdesk.service.implementations;

import com.epamtraining.helpdesk.enums.UserRole;
import com.epamtraining.helpdesk.model.User;
import com.epamtraining.helpdesk.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("UserDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.loadUserByUsername(s);

        if (user != null) {
            List<GrantedAuthority> grantedAuthorities = buildUserAuthority(user);
            return buildUserForAuthentication(user, grantedAuthorities);
        }
        throw new UsernameNotFoundException("User not found!");
    }

    private List<GrantedAuthority> buildUserAuthority(User user) {
        UserRole role = user.getRole();
        GrantedAuthority authority = new SimpleGrantedAuthority(role.toString());
        List<GrantedAuthority> result = new ArrayList<>();
        result.add(authority);
        return result;
    }

    private org.springframework.security.core.userdetails.User buildUserForAuthentication(User user,
                                                                                          List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(
            user.getEmail(), user.getPassword(), true,
            true, true,
            true,
            authorities
        );
    }
}
