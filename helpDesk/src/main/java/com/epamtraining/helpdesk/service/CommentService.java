package com.epamtraining.helpdesk.service;

import com.epamtraining.helpdesk.model.Comment;
import java.util.List;

public interface CommentService {

    void save(int ticketId, String username, String text);

    List<Comment> loadCommentsByTicketId(int ticketId);

}
