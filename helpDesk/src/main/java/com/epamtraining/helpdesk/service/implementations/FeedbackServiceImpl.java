package com.epamtraining.helpdesk.service.implementations;

import com.epamtraining.helpdesk.excetpions.InvalidDataException;
import com.epamtraining.helpdesk.model.Feedback;
import com.epamtraining.helpdesk.model.Ticket;
import com.epamtraining.helpdesk.repository.FeedbackRepository;
import com.epamtraining.helpdesk.service.FeedbackService;
import com.epamtraining.helpdesk.service.NotificationService;
import com.epamtraining.helpdesk.service.TicketService;
import com.epamtraining.helpdesk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    private TicketService ticketService;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    @Qualifier("hostAddress")
    private String hostAddress;

    @Override
    public void save(Feedback feedback, String userName, int ticketId) {
        Ticket ticket = ticketService.loadTicketById(ticketId);
        if(ticket.getOwner().getEmail().equals(userName)){
            feedback.setTicket(ticket);
            feedback.setUser(userService.loadUserByUsername(userName));
            feedbackRepository.save(feedback);
            notificationService.notifyEngineerAboutFeedbackProvided(ticket.getAssignee(), ticketId, hostAddress);

        }
    }

    @Override
    public Feedback loadFeedbackByTicketId(int ticketId, String userName) throws InvalidDataException {
        Ticket ticket = ticketService.loadTicketById(ticketId);
        if(ticket.getOwner().getEmail().equals(userName) || ticket.getAssignee().getEmail().equals(userName)){
            return feedbackRepository.loadFeedbackByTicketId(ticketId);
        }
        else {
            throw new InvalidDataException("Invalid User Name!");
        }
    }
}
