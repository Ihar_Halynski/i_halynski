package com.epamtraining.helpdesk.service;

import com.epamtraining.helpdesk.model.Ticket;
import com.epamtraining.helpdesk.excetpions.InvalidDataException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface TicketService {
    void save(Ticket ticket);

    void saveNewTicket(String ticketDtoJson, MultipartFile[] attachments, String username)
            throws InvalidDataException;

    List<Ticket> loadAllTickets();

    Ticket loadTicketById(int id);

    void update(String ticketDtoJson, MultipartFile[] attachments, String username) throws InvalidDataException;

    void approve(int ticketId, String username);

    void decline(int ticketId, String username);

    void cancelForEmployee(int ticketId, String username);

    void cancelForManager(int ticketId, String username);

    void cancelForEngineer(int ticketId, String username);

    int submit(int ticketId, String username);

    void assign(int ticketId, String username);

    int done(int ticketId, String username);
}
