package com.epamtraining.helpdesk.service;

import com.epamtraining.helpdesk.dto.AttachmentDto;
import com.epamtraining.helpdesk.model.Attachment;
import com.epamtraining.helpdesk.model.Ticket;
import com.epamtraining.helpdesk.model.User;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

public interface AttachmentService {

    void save(Attachment attachment);

    List<Attachment> loadAttachmentsByTicketId(int ticketId);

    List<AttachmentDto> getAttachmentsDtoByTicketId(int ticketId);

    Attachment loadAttachmentById(int id);

    void saveAttachments(MultipartFile[] attachments, Ticket ticket, User user);

    int delete(int attachmentId);
}
