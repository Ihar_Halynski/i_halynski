package com.epamtraining.helpdesk.service.implementations;

import com.epamtraining.helpdesk.converters.TicketConverter;
import com.epamtraining.helpdesk.dto.TicketDto;
import com.epamtraining.helpdesk.enums.State;
import com.epamtraining.helpdesk.model.Ticket;
import com.epamtraining.helpdesk.model.User;
import com.epamtraining.helpdesk.repository.TicketRepository;
import com.epamtraining.helpdesk.service.*;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.epamtraining.helpdesk.excetpions.InvalidDataException;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private UserService userService;

    @Autowired
    private Logger logger;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketConverter ticketConverter;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    @Qualifier("hostAddress")
    private String hostAddress;

    @Override
    public void save(Ticket ticket) {
        ticketRepository.save(ticket);
    }

    @Override
    public List<Ticket> loadAllTickets() {
        return ticketRepository.loadAllTickets();
    }

    @Override
    public void saveNewTicket(String ticketDtoJson, MultipartFile[] attachments, String username) throws InvalidDataException {
//        logger.info("JSON!!!!!!!!!!!!!!!!!!: " + ticketDtoJson);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        TicketDto ticketDto = null;
        try {
            ticketDto = objectMapper.readValue(ticketDtoJson, TicketDto.class);
            if (ticketDto != null) {
                User user = userService.loadUserByUsername(username);
                if (user != null) {
                    ObjectNode node = objectMapper.readValue(ticketDtoJson, ObjectNode.class);
                    ticketDto.setCategory(categoryService.loadCategoryById(objectMapper.readValue(ticketDtoJson, ObjectNode.class).
                        get("categoryId").asInt()));
                    ticketDto.setOwner(user);
                } else {
                    return;
                }
                Optional<Ticket> ticketOptional = ticketConverter.convertToEntity(ticketDto);
                Ticket newTicket;
                if (ticketOptional.isPresent()) {
                    newTicket = ticketOptional.get();
                    save(newTicket);
                    historyService.createItemForCreation(newTicket, user);
                    String commentText = objectMapper.readValue(ticketDtoJson, ObjectNode.class).get("comment").asText();
                    if (commentText != null && commentText != "") {
                        commentService.save(newTicket.getId(), username, commentText);
                    }
                    if(attachments != null){
                        attachmentService.saveAttachments(attachments, newTicket, user);
                    }
                    if(newTicket.getState() == State.NEW){
                        notificationService.notifyManagersAboutNewTicket(userService.loadAllManagers(),newTicket.getId(), hostAddress);
                    }
                }
            } else {
                throw new InvalidDataException("Invalid data!");
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public Ticket loadTicketById(int id) {
        return ticketRepository.loadTicketById(id);
    }

    @Override
    public void update(String ticketDtoJson, MultipartFile[] attachments, String username) throws InvalidDataException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        TicketDto ticketDto = null;
        try {
            ticketDto = objectMapper.readValue(ticketDtoJson, TicketDto.class);
            if (ticketDto != null) {
                User user = userService.loadUserByUsername(username);
                if (user != null) {
                    ObjectNode node = objectMapper.readValue(ticketDtoJson, ObjectNode.class);
                    ticketDto.setCategory(categoryService.loadCategoryById(objectMapper.readValue(ticketDtoJson, ObjectNode.class).
                        get("categoryId").asInt()));
                    ticketDto.setOwner(user);
                }
                Optional<Ticket> ticketOptional = ticketConverter.convertToEntity(ticketDto);
                Ticket ticketForUpdate;
                if (ticketOptional.isPresent()) {
                    ticketForUpdate = ticketOptional.get();
                    ticketRepository.update(ticketForUpdate);
                    historyService.createItemForChangingStatus(ticketForUpdate, user, ticketForUpdate.getState(),
                        this.loadTicketById(ticketForUpdate.getId()).getState());
                    String commentText = objectMapper.readValue(ticketDtoJson, ObjectNode.class).get("comment").asText();
                    if (commentText != null && commentText != "") {
                        commentService.save(ticketForUpdate.getId(), username, commentText);
                    }
                    if (attachments != null) {
                        attachmentService.saveAttachments(attachments, ticketForUpdate, user);
                    }
                    if (ticketForUpdate.getState() == State.NEW) {
                        notificationService.notifyManagersAboutNewTicket(userService.loadAllManagers(), ticketForUpdate.getId(), hostAddress);
                    }
                }
            } else {
                throw new InvalidDataException("Invalid data!");
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void approve(int ticketId, String username) {
        Ticket ticket = ticketRepository.loadTicketById(ticketId);
        historyService.createItemForChangingStatus(ticket, userService.loadUserByUsername(username), ticket.getState()
            , State.APPROVED);
        ticketRepository.updateStateToApproved(ticketId);
        ticketRepository.updateApprover(username, ticketId);
        List<User> users = userService.loadAllEngineers();
        users.add(userService.loadUserByUsername(ticket.getOwner().getEmail()));
        notificationService.notifyUsersAboutApprovedTicket(users, ticketId, hostAddress);

    }

    @Override
    public void decline(int ticketId, String username) {
        Ticket ticket = ticketRepository.loadTicketById(ticketId);
        historyService.createItemForChangingStatus(ticket, userService.loadUserByUsername(username), ticket.getState()
            , State.DECLINED);
        ticketRepository.updateStateToDecline(ticketId);
        ticketRepository.updateApprover(username, ticketId);
        notificationService.notifyUserAboutDeclinedTicket(ticket.getOwner(), ticketId, hostAddress);

    }

    @Override
    public void cancelForEmployee(int ticketId, String username) {
        Ticket ticket = ticketRepository.loadTicketById(ticketId);
        historyService.createItemForChangingStatus(ticket, userService.loadUserByUsername(username), ticket.getState()
            , State.CANCELED);
        ticketRepository.updateStateToCancel(ticketId);
    }

    @Override
    public void cancelForManager(int ticketId, String username) {
        Ticket ticket = ticketRepository.loadTicketById(ticketId);
        historyService.createItemForChangingStatus(ticket, userService.loadUserByUsername(username), ticket.getState()
            , State.CANCELED);
        ticketRepository.updateStateToCancel(ticketId);
        if (!ticket.getOwner().getEmail().equals(username)) {
            ticketRepository.updateApprover(username, ticketId);
        }
        notificationService.notifyUserAboutCancelledTicketByMaager(ticket.getOwner(), ticketId, hostAddress);
    }

    @Override
    public void cancelForEngineer(int ticketId, String username) {
        Ticket ticket = ticketRepository.loadTicketById(ticketId);
        historyService.createItemForChangingStatus(ticket, userService.loadUserByUsername(username), ticket.getState()
            , State.CANCELED);
        ticketRepository.updateStateToCancel(ticketId);
        ticketRepository.updateAssignee(username, ticketId);
        List<User> users = new ArrayList<>();
        users.add(ticket.getApprover());
        users.add(ticket.getOwner());
        notificationService.notifyUsersAboutCancelledTicketByEngineer(users, ticketId, hostAddress);
    }

    @Override
    public int submit(int ticketId, String username) {
        Ticket ticket = ticketRepository.loadTicketById(ticketId);
        historyService.createItemForChangingStatus(ticket, userService.loadUserByUsername(username), ticket.getState()
            , State.NEW);
            notificationService.notifyManagersAboutNewTicket(userService.loadAllManagers(), ticketId, hostAddress);
        return ticketRepository.updateStateToSubmit(ticketId);
    }

    @Override
    public void assign(int ticketId, String username) {
        Ticket ticket = ticketRepository.loadTicketById(ticketId);
        historyService.createItemForChangingStatus(ticket, userService.loadUserByUsername(username), ticket.getState()
            , State.IN_PROGRESS);
        ticketRepository.updateStateToInProgress(ticketId);
        ticketRepository.updateAssignee(username, ticketId);
    }

    @Override
    public int done(int ticketId, String username) {
        Ticket ticket = ticketRepository.loadTicketById(ticketId);
        historyService.createItemForChangingStatus(ticket, userService.loadUserByUsername(username), ticket.getState()
            , State.DONE);
        notificationService.notifyUserAboutDoneTicket(ticket.getOwner(), ticketId, hostAddress);
        return ticketRepository.updateStateToDone(ticketId);
    }
}
