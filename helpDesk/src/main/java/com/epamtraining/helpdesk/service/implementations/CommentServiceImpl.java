package com.epamtraining.helpdesk.service.implementations;

import com.epamtraining.helpdesk.model.Comment;
import com.epamtraining.helpdesk.repository.CommentRepository;
import com.epamtraining.helpdesk.service.CommentService;
import com.epamtraining.helpdesk.service.TicketService;
import com.epamtraining.helpdesk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private UserService userService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public void save(int ticketId, String username, String text) {
        Comment comment = new Comment();
        comment.setText(text);
        comment.setTicket(ticketService.loadTicketById(ticketId));
        comment.setUser(userService.loadUserByUsername(username));
        commentRepository.save(comment);
    }

    @Override
    public List<Comment> loadCommentsByTicketId(int ticketId) {
       return commentRepository.loadCommentsByTicketId(ticketId);
    }
}
