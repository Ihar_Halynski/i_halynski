package com.epamtraining.helpdesk.service.implementations;

import com.epamtraining.helpdesk.model.User;
import com.epamtraining.helpdesk.service.NotificationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private Logger logger;

    @Autowired
    private Environment env;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    @Qualifier("ticketOverviewPatternAddress")
    private String ticketOverviewPatternAddress;

    @Override
    public boolean sendEmail(String template, String subject, List<User> recipients, int ticketId, String from) {
        Context context = new Context();
        context.setVariable("ticketId", ticketId);
        context.setVariable("ticketOverviewPattern", ticketOverviewPatternAddress);
        MimeMessage message = this.emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setFrom(from);
            helper.setSubject(subject);
            ExecutorService emailExecutorService = Executors.newSingleThreadExecutor();
            emailExecutorService.execute(() -> {
                try {
                    for(User recipient : recipients) {
                        context.setVariable("recipient", recipient);
                        helper.setTo(recipient.getEmail());
                        final String html = this.templateEngine.process(template, context);
                        helper.setText(html , true);
                        this.emailSender.send(message);
                    }
                } catch (MessagingException e) {
                    logger.error(e.getMessage());
                }
            });
            emailExecutorService.shutdown();
            return true;
        } catch (MessagingException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean notifyManagersAboutNewTicket(List<User> managers, int ticketId, String from) {
       return sendEmail("email/new-ticket-for-approval", "New ticket for approval"
        , managers, ticketId, from);
    }

    @Override
    public boolean notifyUsersAboutApprovedTicket(List<User> users, int ticketId, String from) {
       return sendEmail("email/ticket-was-approved", "Ticket was approved"
            , users, ticketId, from);
    }

    @Override
    public boolean notifyUserAboutDeclinedTicket(User user, int ticketId, String from) {
        List<User> recipients = new ArrayList<>();
        recipients.add(user);
       return sendEmail("email/ticket-was-declined", "Ticket was declined"
            , recipients, ticketId, from);
    }

    @Override
    public boolean notifyUserAboutCancelledTicketByMaager(User user, int ticketId, String from) {
        List<User> recipients = new ArrayList<>();
        recipients.add(user);
       return sendEmail("email/ticket-was-cancelled", "Ticket was cancelled"
            , recipients, ticketId, from);
    }

    @Override
    public boolean notifyUsersAboutCancelledTicketByEngineer(List<User> users, int ticketId, String from) {
        return sendEmail("email/ticket-was-cancelled-by-engineer", "Ticket was cancelled"
            , users, ticketId, from);
    }

    @Override
    public boolean notifyUserAboutDoneTicket(User user, int ticketId, String from) {
        List<User> recipients = new ArrayList<>();
        recipients.add(user);
        return sendEmail("email/ticket-was-done", "Ticket was done"
            , recipients, ticketId, from);
    }

    @Override
    public boolean notifyEngineerAboutFeedbackProvided(User user, int ticketId, String from) {
        List<User> recipients = new ArrayList<>();
        recipients.add(user);
        return sendEmail("email/feedback-was-provided", "Feedback was provided"
            , recipients, ticketId, from);
    }
}
