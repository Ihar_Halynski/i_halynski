package com.epamtraining.helpdesk.service.implementations;

import com.epamtraining.helpdesk.model.User;
import com.epamtraining.helpdesk.repository.UserRepository;
import com.epamtraining.helpdesk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User loadUserByUsername(String username) {
        return userRepository.loadUserByUsername(username);
    }

    @Override
    public List<User> loadAllManagers() {
        return userRepository.loadAllManagers();
    }

    @Override
    public List<User> loadAllEngineers() {
        return userRepository.loadAllEngineers();
    }
}
