package com.epamtraining.helpdesk.service;

import com.epamtraining.helpdesk.model.Category;
import java.util.List;

public interface CategoryService {
    List<Category> loadCategories();

    Category loadCategoryById(int id);
}
