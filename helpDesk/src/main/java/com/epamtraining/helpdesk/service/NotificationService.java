package com.epamtraining.helpdesk.service;

import com.epamtraining.helpdesk.model.User;
import java.util.List;

public interface NotificationService {

    boolean sendEmail(String templete, String subject, List<User> recipients, int ticketId, String from);

    boolean notifyManagersAboutNewTicket( List<User> managers, int ticketId, String from);

    boolean notifyUsersAboutApprovedTicket(List<User> users, int ticketId, String from);

    boolean notifyUserAboutDeclinedTicket(User user, int ticketId, String from);

    boolean notifyUserAboutCancelledTicketByMaager(User user, int ticketId, String from);

    boolean notifyUsersAboutCancelledTicketByEngineer(List<User> user, int ticketId, String from);

    boolean notifyUserAboutDoneTicket(User user, int ticketId, String from);

    boolean notifyEngineerAboutFeedbackProvided(User user, int ticketId, String from);



}