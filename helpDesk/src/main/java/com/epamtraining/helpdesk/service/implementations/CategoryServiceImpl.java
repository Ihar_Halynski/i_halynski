package com.epamtraining.helpdesk.service.implementations;

import com.epamtraining.helpdesk.model.Category;
import com.epamtraining.helpdesk.repository.CategoryRepository;
import com.epamtraining.helpdesk.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category loadCategoryById(int id) {
        return categoryRepository.loadCategoryById(id);
    }

    @Override
    public List<Category> loadCategories() {
        return categoryRepository.loadCategories();
    }
}
