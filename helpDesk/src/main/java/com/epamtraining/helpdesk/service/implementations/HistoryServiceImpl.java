package com.epamtraining.helpdesk.service.implementations;

import com.epamtraining.helpdesk.enums.State;
import com.epamtraining.helpdesk.model.HistoryItem;
import com.epamtraining.helpdesk.model.Ticket;
import com.epamtraining.helpdesk.model.User;
import com.epamtraining.helpdesk.repository.HistoryRepository;
import com.epamtraining.helpdesk.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    private HistoryRepository historyRepository;

    @Override
    public List<HistoryItem> loadHistoryByTicketId(int ticketId) {
       return historyRepository.loadItemsByTicketId(ticketId);
    }

    @Override
    public void save(HistoryItem historyItem) {
        historyRepository.save(historyItem);
    }

    @Override
    public boolean createItemForCreation(Ticket ticket, User user) {
        HistoryItem historyItem = new HistoryItem();
        historyItem.setTicket(ticket);
        historyItem.setUser(user);
        historyItem.setAction("Ticket is created");
        historyItem.setDescription("Ticket is created");
        this.save(historyItem);
        return true;
    }

    @Override
    public boolean createItemForChangingStatus(Ticket ticket, User user, State from, State to) {
        HistoryItem historyItem = new HistoryItem();
        historyItem.setTicket(ticket);
        historyItem.setUser(user);
        historyItem.setAction("Ticket Status is changed");
        historyItem.setDescription("Ticket Status is changed from " + from + " to " + to);
        this.save(historyItem);
        return true;
    }

    @Override
    public boolean createItemForAttachingFile(Ticket ticket, User user, String filename) {
        HistoryItem historyItem = new HistoryItem();
        historyItem.setTicket(ticket);
        historyItem.setUser(user);
        historyItem.setAction("File is attached");
        historyItem.setDescription("File is attached: " + filename);
        this.save(historyItem);
        return true;
    }
}
