package com.epamtraining.helpdesk.dto;

import com.epamtraining.helpdesk.model.Ticket;

public class AttachmentDto {
    private int id;
    private byte[] blob;
    private Ticket ticket;
    private String name;
    private String extension;

    public AttachmentDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getBlob() {
        return blob;
    }

    public void setBlob(byte[] blob) {
        this.blob = blob;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public static final class AttachmentDtoBuilder {
        private int id;
        private byte[] blob;
        private Ticket ticket;
        private String name;
        private String extension;

        private AttachmentDtoBuilder() {
        }

        public static AttachmentDtoBuilder aAttachmentDto() {
            return new AttachmentDtoBuilder();
        }

        public AttachmentDtoBuilder withId(int id) {
            this.id = id;
            return this;
        }

        public AttachmentDtoBuilder withTicket(Ticket ticket) {
            this.ticket = ticket;
            return this;
        }

        public AttachmentDtoBuilder withBlob(byte[] blob) {
            this.blob = blob;
            return this;
        }

        public AttachmentDtoBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public AttachmentDtoBuilder withExtension(String extension) {
            this.extension = extension;
            return this;
        }

        public AttachmentDto build() {
            AttachmentDto attachmentDto = new AttachmentDto();
            attachmentDto.setId(id);
            attachmentDto.setTicket(ticket);
            attachmentDto.setBlob(blob);
            attachmentDto.setName(name);
            attachmentDto.setExtension(extension);
            return attachmentDto;
        }
    }
}
