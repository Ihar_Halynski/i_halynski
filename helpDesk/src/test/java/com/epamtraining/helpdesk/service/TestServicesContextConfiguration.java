package com.epamtraining.helpdesk.service;

import com.epamtraining.helpdesk.service.implementations.TicketServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.epamtraining.helpdesk"})
public class TestServicesContextConfiguration {

    @Bean
    public TicketServiceImpl ticketService() {
        TicketServiceImpl ticketService = new TicketServiceImpl();
        return ticketService;
    }
}
