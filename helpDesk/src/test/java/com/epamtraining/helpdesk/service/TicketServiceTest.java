package com.epamtraining.helpdesk.service;

import com.epamtraining.helpdesk.converters.TicketConverter;
import com.epamtraining.helpdesk.dto.TicketDto;
import com.epamtraining.helpdesk.enums.State;
import com.epamtraining.helpdesk.enums.Urgency;
import com.epamtraining.helpdesk.enums.UserRole;
import com.epamtraining.helpdesk.excetpions.InvalidDataException;
import com.epamtraining.helpdesk.model.Category;
import com.epamtraining.helpdesk.model.Ticket;
import com.epamtraining.helpdesk.model.User;
import com.epamtraining.helpdesk.repository.implementations.TicketRepositoryImpl;
import com.epamtraining.helpdesk.service.implementations.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.any;

@ExtendWith({MockitoExtension.class})
@RunWith(JUnitPlatform.class)
@ContextConfiguration(classes = TestServicesContextConfiguration.class)
class TicketServiceTest {

    @Autowired
    @InjectMocks
    private TicketServiceImpl ticketService;

    @Mock
    private TicketRepositoryImpl ticketRepository;

    @Mock
    private CategoryServiceImpl categoryService;

    @Mock
    private CommentServiceImpl commentService;

    @Mock
    private UserServiceImpl userService;

    @Mock
    private TicketConverter ticketConverter;

    @Mock
    private HistoryServiceImpl historyService;

    @Mock
    private NotificationServiceImpl notificationService;

    private Ticket ticket;

    private User user;

    private Category category;

    @BeforeEach
    void init() {
        user = new User();
        user.setEmail("email");
        user.setFirstName("FirstName");
        user.setLastName("LastName");
        user.setPassword("password");
        user.setRole(UserRole.EMPLOYEE);
        ticket = new Ticket();
        ticket.setName("ticket1");
        ticket.setCategory(category);
        ticket.setOwner(user);
        ticket.setState(State.NEW);
        ticket.setDesiredResolutionDate(new Date(new Date().getTime()));
        ticket.setDescription("");
        ticket.setUrgency(Urgency.CRITICAL);
        ticket.setId(1);
        category = new Category();
        category.setName("Application & Services");
        user = new User();
        user.setRole(UserRole.EMPLOYEE);
        user.setPassword("");
        user.setFirstName("");
        user.setLastName("");
        user.setEmail("");
        user.setId(1);
    }

    @Test
    void save() {
        ticketService.save(ticket);
        verify(ticketRepository).save(any(Ticket.class));
    }

    @Test
    void saveNewTicket() throws InvalidDataException {
        String username = "";
        MultipartFile[] attachments = null;
        String ticketDtoJson = "{\"state\":\"NEW\",\"categoryId\":1,\"name\":\"ticket1\"," +
            "\"urgency\":\"Critical\",\"description\":\"description\",\"desiredResolutionDate\":1599166800000,\"comment\":\"comment\"}";
        when(categoryService.loadCategoryById(1)).thenReturn(category);
        when(userService.loadUserByUsername("")).thenReturn(user);
        when(ticketConverter.convertToEntity(any(TicketDto.class))).thenReturn(Optional.of(ticket));
        ticketService.saveNewTicket(ticketDtoJson, attachments, username);
        verify(ticketRepository).save(any(Ticket.class));
        verify(historyService).createItemForCreation(any(Ticket.class), any(User.class));
        verify(commentService).save(anyInt(), anyString(), anyString());
        verify(notificationService).notifyManagersAboutNewTicket(anyList(), anyInt(), any());
    }

    @Test
    void loadAllTickets() {
        List<Ticket> tickets = new ArrayList<>();
        tickets.add(ticket);
        List<Ticket> expected = new ArrayList<>();
        expected.add(ticket);
        when(ticketRepository.loadAllTickets()).thenReturn(tickets);
        Assertions.assertArrayEquals(expected.toArray(), ticketService.loadAllTickets().toArray());
        verify(ticketRepository).loadAllTickets();
    }

    @Test
    void loadTicketById() {
        when(ticketRepository.loadTicketById(anyInt())).thenReturn(ticket);
        ticketService.loadTicketById(1);
        verify(ticketRepository).loadTicketById(anyInt());
    }

    @Test
    void update() throws InvalidDataException {
        String username = "";
        MultipartFile[] attachments = null;
        String ticketDtoJson = "{\"state\":\"NEW\",\"categoryId\":1,\"name\":\"ticket1\"," +
            "\"urgency\":\"Critical\",\"description\":\"description\",\"desiredResolutionDate\":1599166800000,\"comment\":\"comment\"}";
        when(categoryService.loadCategoryById(1)).thenReturn(category);
        when(ticketRepository.loadTicketById(anyInt())).thenReturn(ticket);
        when(userService.loadUserByUsername("")).thenReturn(user);
        when(ticketConverter.convertToEntity(any(TicketDto.class))).thenReturn(Optional.of(ticket));
        ticketService.update(ticketDtoJson, attachments, username);
        verify(ticketRepository).update(any(Ticket.class));
        verify(historyService).createItemForChangingStatus(any(Ticket.class), any(User.class), any(State.class), any(State.class));
        verify(commentService).save(anyInt(), anyString(), anyString());
        verify(notificationService).notifyManagersAboutNewTicket(anyList(), anyInt(), any());


    }

    @Test
    void approve() {
        when(ticketRepository.loadTicketById(anyInt())).thenReturn(ticket);
        when(userService.loadUserByUsername("")).thenReturn(user);
        ticketService.approve(1, "");
        verify(historyService).createItemForChangingStatus(any(Ticket.class), any(User.class), any(State.class), any(State.class));
        verify(ticketRepository).updateStateToApproved(anyInt());
        verify(ticketRepository).updateApprover(anyString(), anyInt());
        verify(userService).loadAllEngineers();
        verify(notificationService).notifyUsersAboutApprovedTicket(anyList(), anyInt(), any());

    }

    @Test
    void decline() {
        when(ticketRepository.loadTicketById(anyInt())).thenReturn(ticket);
        when(userService.loadUserByUsername("")).thenReturn(user);
        ticketService.decline(1, "");
        verify(historyService).createItemForChangingStatus(any(Ticket.class), any(User.class), any(State.class)
            , any(State.class));
        verify(ticketRepository).updateStateToDecline(anyInt());
        verify(ticketRepository).updateApprover(anyString(), anyInt());
        verify(notificationService).notifyUserAboutDeclinedTicket(any(User.class), anyInt(), any());
    }

    @Test
    void cancelForEmployee() {
        when(ticketRepository.loadTicketById(anyInt())).thenReturn(ticket);
        when(userService.loadUserByUsername("")).thenReturn(user);
        ticketService.cancelForEmployee(1, "");
        verify(ticketRepository).loadTicketById(anyInt());
        verify(historyService).createItemForChangingStatus(any(Ticket.class), any(User.class), any(State.class)
            , any(State.class));
        verify(ticketRepository).updateStateToCancel(anyInt());
    }

    @Test
    void cancelForManager() {
        when(ticketRepository.loadTicketById(anyInt())).thenReturn(ticket);
        when(userService.loadUserByUsername("")).thenReturn(user);
        ticketService.cancelForManager(1, "");
        verify(ticketRepository).loadTicketById(anyInt());
        verify(historyService).createItemForChangingStatus(any(Ticket.class), any(User.class), any(State.class)
            , any(State.class));
        verify(ticketRepository).updateStateToCancel(anyInt());
        verify(ticketRepository).updateApprover(anyString(), anyInt());
        verify(notificationService).notifyUserAboutCancelledTicketByMaager(any(User.class), anyInt(), any());
    }

    @Test
    void cancelForEngineer() {
        when(ticketRepository.loadTicketById(anyInt())).thenReturn(ticket);
        when(userService.loadUserByUsername("")).thenReturn(user);
        ticketService.cancelForEngineer(1, "");
        verify(ticketRepository).loadTicketById(anyInt());
        verify(historyService).createItemForChangingStatus(any(Ticket.class), any(User.class), any(State.class)
            , any(State.class));
        verify(ticketRepository).updateStateToCancel(anyInt());
        verify(ticketRepository).updateAssignee(anyString(), anyInt());
        verify(notificationService).notifyUsersAboutCancelledTicketByEngineer(anyList(), anyInt(), any());
    }

    @Test
    void submit() {
        when(ticketRepository.loadTicketById(anyInt())).thenReturn(ticket);
        when(userService.loadUserByUsername("")).thenReturn(user);
        ticketService.submit(1, "");
        verify(ticketRepository).loadTicketById(anyInt());
        verify(historyService).createItemForChangingStatus(any(Ticket.class), any(User.class), any(State.class)
            , any(State.class));
        verify(notificationService).notifyManagersAboutNewTicket(anyList(), anyInt(), any());
        verify(ticketRepository).updateStateToSubmit(anyInt());
    }

    @Test
    void assign() {
        when(ticketRepository.loadTicketById(anyInt())).thenReturn(ticket);
        when(userService.loadUserByUsername("")).thenReturn(user);
        ticketService.assign(1, "");
        verify(ticketRepository).loadTicketById(anyInt());
        verify(historyService).createItemForChangingStatus(any(Ticket.class), any(User.class), any(State.class)
            , any(State.class));
        verify(ticketRepository).updateStateToInProgress(anyInt());
        verify(ticketRepository).updateAssignee(anyString(), anyInt());
    }

    @Test
    void done() {
        when(ticketRepository.loadTicketById(anyInt())).thenReturn(ticket);
        when(userService.loadUserByUsername("")).thenReturn(user);
        ticketService.done(1, "");
        verify(ticketRepository).loadTicketById(anyInt());
        verify(historyService).createItemForChangingStatus(any(Ticket.class), any(User.class), any(State.class)
            , any(State.class));
        verify(notificationService).notifyUserAboutDoneTicket(any(User.class), anyInt(), any());
        verify(ticketRepository).updateStateToDone(anyInt());
    }
}
