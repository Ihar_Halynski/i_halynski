package com.homework14.app.task1;

import com.homework14.app.Client;
import com.homework14.app.JsonReader;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

public class HttpURLConnectionClient implements Client {
    private String url;

    public HttpURLConnectionClient(String url) {
        this.url = url;
    }

    @Override
    public String get(int id) {
        String result = null;
        String url = this.url + "/" + id;
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), Charset.forName("UTF-8")));
            JSONObject response = JsonReader.readJsonFromUrl(in);
            result = "Article [1]" + ": User [" + response.get("id").toString() + "] Title [" + response.getString("title") +
                    "] Message [" + response.getString("body") + "]";
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String post(int id) {
        String result = null;
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestMethod("POST");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userId", 1);
            jsonObject.put("title", "some title");
            jsonObject.put("body", "some message");
            jsonObject.put("id", id);
            con.setDoOutput(true);
            DataOutputStream dos = new DataOutputStream(con.getOutputStream());
            dos.writeBytes(jsonObject.toString());
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), Charset.forName("UTF-8")));
            JSONObject response = JsonReader.readJsonFromUrl(in);
            result = "Article [" + response.getInt("id") + "] has been created: User [1] Title [" + response.getString("title") + "] Message [" + response.getString("body") + "]";
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
