package com.homework14.app;

import com.homework14.app.task1.HttpURLConnectionClient;
import com.homework14.app.task2.ApacheHttpClient;

public class App {
    private static String url = "http://jsonplaceholder.typicode.com/posts";

    public static void printWrongArguments() {
        System.out.println("Wrong arguments!");
    }

    public static void main(String[] args) {
        try {
            HttpURLConnectionClient httpURLConnectionClient = new HttpURLConnectionClient(url);
            ApacheHttpClient apacheHttpClient = new ApacheHttpClient(url);
            if (args.length == 3) {
                String getOrPost = args[0];
                int id = Integer.parseInt(args[1]);
                int method = Integer.parseInt(args[2]);
                if (getOrPost.equals("GET") && id > 0 && method == 1) {
                    System.out.println(httpURLConnectionClient.get(id));
                } else if (getOrPost.equals("POST") && id > 0 && method == 1) {
                    System.out.println(httpURLConnectionClient.post(id));
                } else if (getOrPost.equals("GET") && id > 0 && method == 2) {
                    System.out.println(apacheHttpClient.get(id));
                } else if (getOrPost.equals("POST") && id > 0 && method == 2) {
                    System.out.println(apacheHttpClient.post(id));
                } else {
                    printWrongArguments();
                }
            } else {
                printWrongArguments();
            }
        } catch (NumberFormatException e){
            printWrongArguments();
        }

    }
}
