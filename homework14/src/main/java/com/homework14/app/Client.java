package com.homework14.app;

public interface Client {

    String get(int id);

    String post(int id);
}
