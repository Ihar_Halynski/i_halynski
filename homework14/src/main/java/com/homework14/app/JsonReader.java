package com.homework14.app;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;

public class JsonReader {
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(BufferedReader rd) throws IOException, JSONException {
        String jsonText = readAll(rd);
        JSONObject json = new JSONObject(jsonText);
        return json;
    }
}
