package com.homework14.app.task2;

import com.homework14.app.Client;
import com.homework14.app.JsonReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ApacheHttpClient implements Client {
    private String url;

    public ApacheHttpClient(String url) {
        this.url = url;
    }

    @Override
    public String get(int id) {
        String url = this.url + "/" + id;
        String resultString = null;
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);
            HttpResponse response = client.execute(request);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));
            JSONObject responseObject = JsonReader.readJsonFromUrl(in);
            resultString = "Article [1]" + ": User [" + responseObject.get("id").toString() + "] Title [" + responseObject.getString("title") +
                    "] Message [" + responseObject.getString("body") + "]";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }

    @Override
    public String post(int id) {
        String resultString = null;
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            StringEntity stringEntity = new StringEntity("{\"userId\": 1,\"title\": \"some title\", \"body\": \"some message\", \"id\":" + id + "}");
            post.addHeader("content-type", "application/json");
            post.setEntity(stringEntity);
            HttpResponse response = client.execute(post);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));
            JSONObject responseObject = JsonReader.readJsonFromUrl(in);
            resultString = "Article [" + responseObject.getInt("id") + "] has been created: User [1] Title [" + responseObject.getString("title") + "] Message [" + responseObject.getString("body") + "]";
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }
}
