package homework2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FactorialTest {

    @Test
    public void testCalculateUsingWhileTest() {
        int actual = Factorial.calculateUsingWhile(4);
        assertEquals(24, actual);
    }

    @Test
    public void testCalculateUsingForTest() {
        int actual = Factorial.calculateUsingFor(4);
        assertEquals(24, actual);
    }

    @Test
    public void testCalculateUsingDoWhileTest() {
        int actual = Factorial.calculateUsingDoWhile(4);
        assertEquals(24, actual);
    }
}