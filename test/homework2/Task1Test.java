package homework2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task1Test {

    @Test
    public void testCalculateG() {
        double actual = Task1.calculateG(1, 1, 1, 1);
        assertEquals(19.7, actual, 0.1);
    }
}