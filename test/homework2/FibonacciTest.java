package homework2;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class FibonacciTest {
    private int[] checkingArray = {1, 1, 2, 3, 5, 8};

    @Test
    public void testCalculateUsingWhile() {
        int[] actual = Fibonacci.calculateUsingWhile(6);
        assertArrayEquals(checkingArray, actual);
    }

    @Test
    public void testCalculateUsingForTest() {
        int[] actual = Fibonacci.calculateUsingFor(6);
        assertArrayEquals(checkingArray, actual);
    }

    @Test
    public void testCaclulateUsingDoWhileTest() {
        int[] actual = Fibonacci.calculateUsingDoWhile(6);
        assertArrayEquals(checkingArray, actual);
    }
}