package homework9.task1;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ComparatorBySumOfNumbersTest {
    private static Integer[] array;

    @Before
    public void setUp() {
        array = new Integer[]{-55, 13, 12, 31, 21, 55};
    }

    @Test
    public void testGetSumOfSymbols() {
        int result = ComparatorBySumOfNumbers.getSumOfSymbols(43);
        assertEquals(7, result);
    }

    @Test
    public void testCompare() {
        Arrays.sort(array, new ComparatorBySumOfNumbers());
        assertArrayEquals(new Integer[]{12, 21, 13, 31, -55, 55}, array);
    }
}