package homework9.task2;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.assertArrayEquals;

public class SetOperationTest {
    private static HashSet<Integer> set1;
    private static HashSet<Integer> set2;

    @Before
    public void setUp() {
        set1 = new HashSet<>();
        set1.add(new Integer(1));
        set1.add(new Integer(2));
        set1.add(new Integer(3));
        set1.add(new Integer(4));
        set2 = new HashSet<>();
        set2.add(new Integer(1));
        set2.add(new Integer(3));
        set2.add(new Integer(5));
        set2.add(new Integer(6));
    }

    @Test
    public void testUnion() {
        HashSet<Integer> setResult = SetOperation.union(set1, set2);
        Integer[] result = setResult.toArray(new Integer[]{});
        assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6}, result);
    }

    @Test
    public void testIntersection() {
        HashSet<Integer> setResult = SetOperation.intersection(set1, set2);
        Integer[] result = setResult.toArray(new Integer[]{});
        assertArrayEquals(new Integer[]{1, 3}, result);
    }

    @Test
    public void testDifference() {
        HashSet<Integer> setResult = SetOperation.difference(set1, set2);
        Integer[] result = setResult.toArray(new Integer[]{});
        assertArrayEquals(new Integer[]{2, 4}, result);
    }

    @Test
    public void testExclusion() {
        HashSet<Integer> setResult = SetOperation.exclusion(set1, set2);
        Integer[] result = setResult.toArray(new Integer[]{});
        assertArrayEquals(new Integer[]{2, 4, 5, 6}, result);
    }
}