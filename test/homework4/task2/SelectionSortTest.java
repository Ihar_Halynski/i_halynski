package homework4.task2;

import homework4.task2.SelectionSort;
import org.junit.Test;
import org.junit.Before;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runners.Parameterized;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SelectionSortTest {

    private int[] inputArray;
    private int[] expectedResult;
    private SelectionSort selectionSort;

    @Before
    public void initialize() {
        selectionSort = new SelectionSort();
    }

    public SelectionSortTest(int[] inputArray, int[] expectedResult) {
        this.inputArray = inputArray;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection arrays() {
        return Arrays.asList(new Object[][]{
                {new int[]{1, 3, 5, 0}, new int[]{0, 1, 3, 5}},
                {new int[]{10, 2, 5, 1}, new int[]{1, 2, 5, 10}}
        });
    }

    @Test
    public void testSort() {
        System.out.println("Parameterized array is : ");
        for (int i = 0; i < inputArray.length; i++) {
            System.out.print(inputArray[i] + ", ");
        }
        System.out.println();
        assertArrayEquals(expectedResult, selectionSort.sort(inputArray));
    }
}