package homework4.task1;

import homework4.task1.CreditCard;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreditCardTest {

    private static CreditCard testCard;
    private static CreditCard testCardBalance0;

    @Before
    public void init() {
        testCard = new CreditCard("name", 100);
        testCardBalance0 = new CreditCard("name", 0);
    }

    @Test
    public void testGetBalance() {
        double result = testCard.getBalance();
        assertEquals(100, result, 0);
    }

    @Test
    public void testAddMoney() {
        double result = testCard.addMoney(10);
        assertEquals(110, result, 0);
    }

    @Test
    public void testTakeMoney() {
        double result = testCard.takeMoney(10);
        assertEquals(90, result, 0);
    }

    @Test
    public void testTakeMoneyWhenBalance0() {
        double result = testCardBalance0.takeMoney(10);
        assertEquals(-10, result, 0);
    }
}