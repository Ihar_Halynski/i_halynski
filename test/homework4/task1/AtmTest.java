package homework4.task1;

import homework4.task1.Atm;
import homework4.task1.CreditCard;
import homework4.task1.DebitCard;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AtmTest {
    private static Atm testAtmWithCredCard;
    private static Atm testAtmWithDebitCard;

    @Before
    public void setUp() throws Exception {
        testAtmWithCredCard = new Atm(new CreditCard("name", 0), 100);
        testAtmWithDebitCard = new Atm(new DebitCard("name", 10), 100);
    }

    @Test
    public void testAddMoney_to_cred_card() {
        double result = testAtmWithCredCard.addMoney(10);
        assertEquals(10, result, 0);

    }

    @Test
    public void testAddMoney_to_debit_card() {
        double result = testAtmWithDebitCard.addMoney(10);
        assertEquals(20, result, 0);
    }

    @Test
    public void testTakeCash_from_cred_card() {
        double result = testAtmWithCredCard.takeCash(10);
        assertEquals(-10, result, 0);
    }

    @Test
    public void testTakeCash_from_debit_card() {
        double result = testAtmWithDebitCard.takeCash(5);
        assertEquals(5, result, 0);
    }

    @Test
    public void testTakeCash_deb_card_when_cash_less_amount() {
        double result = testAtmWithDebitCard.takeCash(150);
        assertEquals(0, result, 0);
    }

    @Test
    public void testTakeCash_cred_card_when_cash_less_amount() {
        double result = testAtmWithCredCard.takeCash(150);
        assertEquals(0, result, 0);
    }

}