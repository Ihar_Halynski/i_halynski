package homework6;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringManagerTest {

    @Test
    public void testProcessString() {
        String testString = "aa ab ka ba bb ad kb ca aa cb ak da db da ";
        String result = StringManager.processString(testString);
        String expected = "aa ab ad ak \nba bb \nca cb \nda db \nka kb \n";
        assertEquals(expected, result);
    }

    @Test
    public void testProcessString_with_punctuation_marks() {
        String testString = "aa, ab! ka? ba. bb .kb ca ,aa cb da .db da ";
        String result = StringManager.processString(testString);
        String expected = "aa ab \nba bb \nca cb \nda db \nka kb \n";
        assertEquals(expected, result);
    }

    @Test
    public void testGroupWords() {
        String[] testArray = new String[]{"aa", "ba", "ab", "ca", "da"};
        String result = StringManager.groupWords(testArray);
        String expected = "aa ab \nba \nca \nda \n";
        assertEquals(expected, result);
    }
}