package com.servlets;

import java.util.HashMap;

public class Product {
    private String name;
    private Double price;

    public String getName() {
        return name;
    }

    public Product(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return name + " " + price + "$";
    }

    public static HashMap<String, Double> getAvailableProducts() {
        HashMap<String, Double> products = new HashMap<>();
        products.put("Book1", 10d);
        products.put("Book2", 20d);
        products.put("Car1", 5.5);
        products.put("Car2", 2.2);
        return products;
    }
}
