package com.servlets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Catalog {
    private HashSet<Product> availableProducts = new HashSet<>();

    public boolean addProduct(String name, Double price) {
        Product product = new Product(name, price);
        return availableProducts.add(product);
    }

    public HashSet<Product> getAvailableProducts() {
        return availableProducts;
    }

    public ArrayList<Product> getAvailableProductsAsStringList() {
        ArrayList<Product> result = new ArrayList<>();
        for (Product p : availableProducts) {
            result.add(p);
        }
        return result;
    }

    public Product getProductByName(String name) {
        for (Product p : availableProducts) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }
}
