package com.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ForwardServlet extends DispatcherServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String submit = request.getParameter("submit");
        String addItem = request.getParameter("addItem");
        List<Product> selectedProducts = null;
        if (submit != null) {
            selectedProducts = (ArrayList<Product>) session.getAttribute("selectedProducts");
            double total = 0;
            if (selectedProducts != null) {
                total = calculateTotal(selectedProducts);
            }
            session.setAttribute("total", total);
            forward("/checkpage.jsp", request, response);
        } else if (addItem != null) {
            Catalog catalog = (Catalog) session.getAttribute("catalog");
            Product selectedProduct = null;
            String selected = request.getParameter("product");
            if (catalog != null && selected != null) {
                selectedProduct = catalog.getProductByName(selected);
            }
            if (selectedProduct != null) {
                selectedProducts = (ArrayList<Product>) session.getAttribute("selectedProducts");
                if (selectedProducts != null) {
                    selectedProducts.add(selectedProduct);
                } else {
                    selectedProducts = new ArrayList<>();
                    selectedProducts.add(selectedProduct);
                }
                session.setAttribute("selectedProducts", selectedProducts);
            }
            super.forward("/catalogpage.jsp", request, response);
        } else {
            super.forward("/errorpage.jsp", request, response);
        }
    }

    private double calculateTotal(List<Product> products) {
        double result = 0;
        for (Product p : products) {
            result = result + p.getPrice();
        }
        return result;
    }
}
