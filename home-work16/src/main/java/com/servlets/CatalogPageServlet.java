package com.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CatalogPageServlet extends DispatcherServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean agreeTermsOfService = Boolean.parseBoolean(request.getParameter("agreeTermsOfService"));
        if (agreeTermsOfService) {
            String userName = request.getParameter("userName");
            HttpSession session = request.getSession();
            if (userName != null) {
                request.getSession(false).invalidate();
                session = request.getSession();
            }
            session.setAttribute("userName", userName);
            Catalog catalog = new Catalog();
            catalog.addProduct("Book1", 10d);
            catalog.addProduct("Car1", 20d);
            catalog.addProduct("Book2", 12d);
            catalog.addProduct("Car2", 4d);
            session.setAttribute("catalog", catalog);
            forward("/catalogpage.jsp", request, response);
        } else {
            forward("/errorpage.jsp", request, response);
        }
    }
}
