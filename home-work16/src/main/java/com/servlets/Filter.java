package com.servlets;

import javax.servlet.*;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class Filter implements javax.servlet.Filter {

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        RequestDispatcher dispatcher = null;
        boolean agreeTermsOfService = Boolean.parseBoolean(req.getParameter("agreeTermsOfService"));
        String submit = req.getParameter("submit");
        String addItem = req.getParameter("addItem");
        if (agreeTermsOfService) {
            dispatcher = req.getRequestDispatcher("catalogpage.jsp");
        } else if (submit!= null) {
            dispatcher = req.getRequestDispatcher("checkpage.jsp");
        } else if (addItem!= null){
            dispatcher = req.getRequestDispatcher("catalogpage.jsp");
        } else {
            dispatcher = req.getRequestDispatcher("errorpage.jsp");
        }
        dispatcher.forward(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {
    }

}
