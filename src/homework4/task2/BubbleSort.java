package homework4.task2;

public class BubbleSort implements Sorter {

    public int[] sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean isSwaped = false;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                    isSwaped = true;
                }
            }
            if (isSwaped == false) {
                break;
            }
        }
        return array;
    }
}
