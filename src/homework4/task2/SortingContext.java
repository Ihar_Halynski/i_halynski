package homework4.task2;

import homework4.task2.Sorter;

public class SortingContext {
    private Sorter sorter;

    public SortingContext(Sorter sorter) {
        this.sorter = sorter;
    }

    public int[] execute(int[] array) {
        return sorter.sort(array);
    }
}
