package homework4.task2;

public interface Sorter {

    public int[] sort(int[] array);
}
