package homework4.task2;

public class MainTask2 {

    public static void main(String[] args) {
        try {
            int arg = Integer.parseInt(args[0]);
            int[] array = new int[]{1, 3, 5, 0};
            Sorter sortStrategy = null;
            if (arg == 1) {
                sortStrategy = new BubbleSort();
            }
            if (arg == 2) {
                sortStrategy = new SelectionSort();
            }
            if (arg < 1 || arg > 2) {
                System.out.println("Wrong arguments");
            }
            SortingContext context = new SortingContext(sortStrategy);
            context.execute(array);
            for (int i = 0; i < array.length; i++) {
                System.out.println(array[i]);
            }
        } catch (NumberFormatException e) {
            System.out.println("Wrong arguments");
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }
}
