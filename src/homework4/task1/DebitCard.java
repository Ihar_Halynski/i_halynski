package homework4.task1;

import homework4.task1.Card;

public class DebitCard extends Card {

    public DebitCard(String ownerName, double initialBalance) {
        super(ownerName, initialBalance);
    }
}
