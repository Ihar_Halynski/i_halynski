package homework4.task1;

public class Atm {
    private Card card;
    private double cash;

    public Atm(Card card, double cash) {
        this.card = card;
        this.cash = cash;
    }

    public double addMoney(double amount) {
        if (card != null) {
            card.addMoney(amount);
            return card.getBalance();
        }
        return 0;
    }

    public double takeCash(double amount) {
        if (card != null) {
            if (amount > cash) {
                return 0;
            }
            if (amount <= cash) {
                cash = cash - amount;
                return card.takeMoney(amount);
            }
        }
        return 0;
    }
}
