package homework4.task1;

import homework4.task1.Card;

public class CreditCard extends Card {

    public CreditCard(String ownerName, double initialBalance) {
        super(ownerName, initialBalance);
    }

    @Override
    public double takeMoney(double amount) {
        balance = balance - amount;
        return balance;
    }
}
