package homework4.task1;

public class Card {
    protected String ownerName;
    protected double balance;

    public Card(String ownerName, double initialBalance) {
        this.ownerName = ownerName;
        this.balance = initialBalance;
    }

    public Card(String ownerName) {
        this(ownerName, 0);
    }

    public double getBalance() {
        return balance;
    }

    public double addMoney(double amount) {
        balance = balance + amount;
        return balance;
    }

    public double takeMoney(double amount) {
        if (amount > balance) {
            return 0;
        } else {
            balance = balance - amount;
            return balance;
        }
    }
}

