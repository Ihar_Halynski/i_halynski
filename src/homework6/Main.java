package homework6;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter text: ");
        String input = sc.nextLine();
        String output = StringManager.processString(input);
        System.out.println("Result:\n" + output);
    }
}
