package homework6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class StringManager {

    public static String processString(String s) {
        s = s.replaceAll("([^A-Za-z]+)", " ").toLowerCase();
        HashSet<String> sSet = new HashSet<String>(Arrays.asList(s.split(" ")));
        String[] sArray = new String[sSet.size()];
        sArray = sSet.toArray(sArray);
        return groupWords(sArray);
    }

    public static String groupWords(String[] array) {
        List<List<String>> sList = new ArrayList<>();
        Arrays.sort(array);
        String tmp = array[0];
        sList.add(new ArrayList<>());
        int j = 0;
        sList.get(j).add(tmp);
        for (int i = 1; i < array.length; i++) {
            if (array[i].charAt(0) == tmp.charAt(0)) {
                sList.get(j).add(array[i]);
            } else {
                sList.add(new ArrayList<>());
                j++;
                sList.get(j).add(array[i]);
                tmp = array[i];
            }
        }
        return convertStringListToString(sList);
    }

    public static String convertStringListToString(List<List<String>> list) {
        StringBuilder sb = new StringBuilder();
        for (List<String> l : list) {
            for (int i = 0; i < l.size(); i++) {
                sb.append(l.get(i) + " ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
