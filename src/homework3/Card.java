package homework3;

public class Card {
    private String ownerName;
    private double balance;

    public Card(String ownerName, double initialBalance) {
        this.ownerName = ownerName;
        this.balance = initialBalance;
    }

    public Card(String ownerName) {
        this(ownerName, 0);
    }

    public double getBalance() {
        return balance;
    }

    public double addMoney(double amount) {
        balance = balance + amount;
        return balance;
    }

    public double takeMoney(double amount) {
        if (amount > balance) {
            return 0;
        } else {
            balance = balance - amount;
            return balance;
        }
    }

    public double getConvertedBalance(double conversionRate) {
        double result = balance * conversionRate;
        return result;
    }
}

