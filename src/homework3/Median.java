package homework3;

import java.util.Arrays;

public class Median {

    public static float median(int[] array) {
        int index;
        float median;
        Arrays.sort(array);
        if (array.length == 0) {
            return 0;
        }
        if ((array.length % 2) == 0) {
            index = array.length / 2 - 1;
            median = (float) (array[index] + array[index + 1]) / 2;
            return median;
        }
        if ((array.length % 2) > 0) {
            index = (array.length) / 2;
            median = array[index];
            return median;
        }
        return 0;
    }

    public static double median(double[] array) {
        int index;
        double median;
        Arrays.sort(array);
        if (array.length == 0) {
            return 0;
        }
        if ((array.length % 2) == 0) {
            index = array.length / 2 - 1;
            median = (array[index] + array[index + 1]) / 2;
            return median;
        }
        if ((array.length % 2) > 0) {
            index = (array.length) / 2;
            median = array[index];
            return median;
        }
        return 0;
    }
}
