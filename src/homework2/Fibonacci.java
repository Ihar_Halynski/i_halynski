package homework2;

public class Fibonacci {

    public static int[] calculateUsingWhile(int n) {
        if (n == 1) {
            return new int[]{1};
        }
        if (n == 2) {
            return new int[]{1, 1};
        }
        if (n > 2) {
            int[] result = new int[n];
            result[0] = 1;
            int counter = 1;
            int sumFib = 0;
            int a = 0;
            int b = 1;
            while (counter < n) {
                sumFib = a + b;
                a = b;
                b = sumFib;
                result[counter] = sumFib;
                counter++;
            }
            return result;
        } else {
            return new int[]{};
        }

    }

    public static int[] calculateUsingFor(int n) {
        if (n == 1) {
            return new int[]{1};
        }
        if (n == 2) {
            return new int[]{1, 1};
        }
        if (n > 2) {
            int[] result = new int[n];
            result[0] = 1;
            int sumFib = 0;
            int a = 0;
            int b = 1;
            for (int i = 1; i < n; i++) {
                sumFib = a + b;
                a = b;
                b = sumFib;
                result[i] = sumFib;
            }
            return result;
        } else {
            return new int[]{};
        }
    }

    public static int[] calculateUsingDoWhile(int n) {
        if (n == 1) {
            return new int[]{1};
        }
        if (n == 2) {
            return new int[]{1, 1};
        }
        if (n > 2) {
            int[] result = new int[n];
            result[0] = 1;
            int counter = 1;
            int sumFib = 0;
            int a = 0;
            int b = 1;
            do {
                sumFib = a + b;
                a = b;
                b = sumFib;
                result[counter] = sumFib;
                counter++;
            } while (counter < n);

            return result;
        } else {
            return new int[]{};
        }
    }
}
