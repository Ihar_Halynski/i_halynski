package homework2;

public class Task1 {

    public static void main(String[] args) {
        int a, p;
        double m1, m2, g;
        try {
            if (args.length == 4) {
                a = Integer.parseInt(args[0]);
                p = Integer.parseInt(args[1]);
                m1 = Double.parseDouble(args[2]);
                m2 = Double.parseDouble(args[3]);
                g = calculateG(a, p, m1, m2);
                System.out.println("G = " + g);
            } else {
                System.out.println("Wrong arguments");
            }
        } catch (NumberFormatException e) {
            System.out.println("Exception " + e.getMessage());
        }
    }

    public static double calculateG(int a, int p, double m1, double m2) {
        double g = 4 * Math.pow(Math.PI, 2) * (Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2)));
        return g;
    }
}
