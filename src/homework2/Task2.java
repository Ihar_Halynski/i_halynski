package homework2;

public class Task2 {

    public static void main(String[] args) {
        try {
            if (args.length == 3) {
                int arg1 = Integer.parseInt(args[0]);
                int arg2 = Integer.parseInt(args[1]);
                int arg3 = Integer.parseInt(args[2]);
                if (arg1 == 1 && arg3 >= 0) {
                    int[] fibArray = new int[]{};
                    if (arg2 == 1) {
                        fibArray = Fibonacci.calculateUsingWhile(arg3);
                    } else if (arg2 == 2) {
                        fibArray = Fibonacci.calculateUsingDoWhile(arg3);
                    } else if (arg2 == 3) {
                        fibArray = Fibonacci.calculateUsingFor(arg3);
                    } else {
                        System.out.println("Wrong arguments");
                    }
                    for (int i = 0; i < fibArray.length; i++) {
                        System.out.println(fibArray[i]);
                    }
                } else if (arg1 == 2 && arg3 >= 0) {
                    int factorial = 0;
                    if (arg2 == 1) {
                        factorial = Factorial.calculateUsingWhile(arg3);
                        System.out.println(factorial);
                    } else if (arg2 == 2) {
                        factorial = Factorial.calculateUsingDoWhile(arg3);
                        System.out.println(factorial);
                    } else if (arg2 == 3) {
                        factorial = Factorial.calculateUsingFor(arg3);
                        System.out.println(factorial);
                    } else {
                        System.out.println("Wrong arguments");
                    }
                } else {
                    System.out.println("Wrong arguments");
                }
            }
        } catch (NumberFormatException e) {
            System.out.println("Exception: " + e.getMessage());
        }

    }
}
