package homework2;

public class Factorial {

    public static int calculateUsingWhile(int n) {
        int counter = 1;
        int result = 1;
        while (counter <= n) {
            result = result * counter;
            counter++;
        }
        return result;
    }

    public static int calculateUsingFor(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }

    public static int calculateUsingDoWhile(int n) {
        int counter = 1;
        int result = 1;
        do {
            result = result * counter;
            counter++;
        } while (counter <= n);
        return result;
    }
}
