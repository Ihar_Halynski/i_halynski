package homework9.task2;

import java.util.HashSet;
import java.util.Set;

public class SetOperation extends HashSet {

    private static <E> void checkForNull(Set<E> set1, Set<E> set2) {
        if (set1 != null && set2 != null) {
            return;
        } else {
            throw new NullPointerException();
        }
    }

    public static <E> HashSet<E> union(Set<E> set1, Set<E> set2) {
        checkForNull(set1, set2);
        HashSet<E> result = new HashSet<>();
        for (E o : set1) {
            result.add(o);
        }
        for (E o : set2) {
            result.add(o);
        }
        return result;

    }

    public static <E> HashSet<E> intersection(Set<E> set1, Set<E> set2) {
        checkForNull(set1, set2);
        HashSet<E> result = new HashSet<>();
        for (E o : set1) {
            if (set2.contains(o)) {
                result.add(o);
            }
        }
        return result;
    }

    public static <E> HashSet<E> difference(Set<E> set1, Set<E> set2) {
        checkForNull(set1, set2);
        HashSet<E> result = new HashSet<>();
        for (E o : set1) {
            if (!set2.contains(o)) {
                result.add(o);
            }
        }
        return result;
    }

    public static <E> HashSet<E> exclusion(Set<E> set1, Set<E> set2) {
        checkForNull(set1, set2);
        HashSet<E> result = new HashSet<>();
        for (E o : set1) {
            if (!set2.contains(o)) {
                result.add(o);
            }
        }
        for (E o : set2) {
            if (!set1.contains(o)) {
                result.add(o);
            }
        }
        return result;
    }
}
