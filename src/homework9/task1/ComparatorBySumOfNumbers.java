package homework9.task1;

import java.util.Comparator;

public class ComparatorBySumOfNumbers implements Comparator {

    public static int getSumOfSymbols(int number) {
        char[] charArray = String.valueOf(number).toCharArray();
        int j = 0;
        if (charArray[0] == '-') {
            j = 1;
        }
        int sum = 0;
        for (int i = j; i < charArray.length; i++) {
            sum = sum + Integer.parseInt(String.valueOf(charArray[i]));
        }
        return sum;
    }

    @Override
    public int compare(Object o1, Object o2) {
        if (o1 != null && o2 != null) {
            if (o1 instanceof Integer && o2 instanceof Integer) {
                int number1 = (int) o1;
                int number2 = (int) o2;
                int sumO1 = getSumOfSymbols(number1);
                int sumO2 = getSumOfSymbols(number2);
                if (sumO1 == sumO2) {
                    return 0;
                }
                if (sumO1 > sumO2) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
        return 0;
    }
}
