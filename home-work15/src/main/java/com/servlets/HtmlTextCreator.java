package com.servlets;

public interface HtmlTextCreator {
    String createHtmlText(String param);
}
