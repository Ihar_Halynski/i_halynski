package com.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class StartPageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print(createHtmlText());
        out.close();
    }

    public String createHtmlText() {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Title</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1 align=\"center\">Welcome to Online Shop</h1>\n" +
                "<center><form action=\"/secondpage\" method=\"post\">\n" +
                "    <input type=\"text\" name=\"username\"/>\n" +
                "    <input type=\"submit\" value=\"Enter\"/>\n" +
                "</form></center>\n" +
                "</body>\n" +
                "</html>";
    }
}
