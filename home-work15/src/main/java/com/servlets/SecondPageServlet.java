package com.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class SecondPageServlet extends javax.servlet.http.HttpServlet implements HtmlTextCreator {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] values = request.getParameterValues("username");
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print(createHtmlText(values[0]));
        out.close();
    }

    public String createHtmlText(String param) {
        StringBuffer sb = new StringBuffer();
        sb.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Title</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1 align=\"center\">Hello " + param + "!</h1>\n" +
                "<h2 align=\"center\">Make you order</h2>\n" +
                "<center><form action=\"/checkpage\" method=\"post\">\n" +
                "    <select multiple name=\"product\">\n");
        HashMap<String, Double> availableProducts = Product.getAvailableProducts();
        for (String key : availableProducts.keySet()) {
            sb.append("<option value=\"" + key + "\">" + key + " (" + availableProducts.get(key) + "$)</option>\n");
        }
        sb.append("    </select>\n" +
                "    <input type=\"submit\" value=\"Enter\"/>\n" +
                "</form></center>\n" +
                "</body>\n" +
                "</html>");
        return sb.toString();

    }
}
