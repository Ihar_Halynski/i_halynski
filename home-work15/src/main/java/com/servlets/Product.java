package com.servlets;

import java.util.HashMap;

public class Product {

    public static HashMap<String, Double> getAvailableProducts() {
        HashMap<String, Double> products = new HashMap<>();
        products.put("Book1", 10d);
        products.put("Book2", 20d);
        products.put("Car1", 5.5);
        products.put("Car2", 2.2);
        return products;
    }
}
