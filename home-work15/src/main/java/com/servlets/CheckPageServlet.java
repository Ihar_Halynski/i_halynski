package com.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class CheckPageServlet extends HttpServlet implements HtmlTextCreator {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] values = request.getParameterValues("product");
        String tableText = "";
        if (values != null) {
            tableText = createCheck(values);
        }
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print(createHtmlText(tableText));
        out.close();
    }

    private String createCheck(String[] param) {
        Double total = 0.0;
        HashMap<String, Double> availableProducts = Product.getAvailableProducts();
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < param.length; i++) {
            double cost = availableProducts.get(param[i]);
            total = total + cost;
            result.append("<h4 align=\"center\">" + (i + 1) + ") " + param[i] + " " + cost + "$</h4>\n");
        }
        result.append("<h4 align=\"center\">Total: $ " + total + "</h4>");
        return result.toString();
    }

    @Override
    public String createHtmlText(String param) {
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Title</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1 align=\"center\">Your order:</h1>\n" +
                param +
                "</body>\n" +
                "</html>";
    }
}
